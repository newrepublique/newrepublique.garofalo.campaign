﻿using AutoMapper;
using NewRepublique.AdminWebsite.Models.DTOs;
using NewRepublique.Data.Service;
using NewRepublique.Infrastructure.Helpers;
using NewRepublique.PetaPoco.Repository.POCOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;

namespace NewRepublique.AdminWebsite.Security
{    
    public class CustomMembershipProvider : MembershipProvider //WebMatrix.WebData.SimpleMembershipProvider
    {
        private IUserService _userService;
        private IHashingHelper _hashingHelper;

        public CustomMembershipProvider()
            : this(DependencyResolver.Current.GetService<IUserService>(), DependencyResolver.Current.GetService<IHashingHelper>())
        {
        }

        public CustomMembershipProvider(IUserService userService, IHashingHelper hashingHelper)
        {
            _userService = userService;
            _hashingHelper = hashingHelper;
        }

        /// <summary>
        /// Is used by Membership.ValidateUser
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public override bool ValidateUser(string username, string password)
        {
            var result = _userService.Get(username, password);
            if (result != null)
            {
                if (!result.Locked)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            var currentHashedPassword = _hashingHelper.HashStringInSHA256(oldPassword);
            var newHashedPassword = _hashingHelper.HashStringInSHA256(newPassword);

            var currentUser = Mapper.Map<User, UserModel>(_userService.Get(username));

            currentUser.Password = newHashedPassword;
            currentUser.LastModifiedOn = DateTime.Now;
            currentUser.LastModifiedBy = username;

            if (_userService.Update(Mapper.Map<UserModel, User>(currentUser)) > 0)
                return true;
            else
                return false;
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        

        protected override byte[] DecryptPassword(byte[] encodedPassword)
        {
            return base.DecryptPassword(encodedPassword);
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        protected override byte[] EncryptPassword(byte[] password)
        {
            return base.EncryptPassword(password);
        }

        protected override byte[] EncryptPassword(byte[] password, System.Web.Configuration.MembershipPasswordCompatibilityMode legacyPasswordCompatibilityMode)
        {
            return base.EncryptPassword(password, legacyPasswordCompatibilityMode);
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override bool EnablePasswordRetrieval
        {
            get { throw new NotImplementedException(); }
        }

        public override bool EnablePasswordReset
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { throw new NotImplementedException(); }
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { throw new NotImplementedException(); }
        }

        public override int PasswordAttemptWindow
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresUniqueEmail
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredPasswordLength
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new NotImplementedException(); }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new NotImplementedException(); }
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            //throw new NotImplementedException();

            var result = _userService.Get(username);
            if (result != null)
            {                
                MembershipUser mu = new MembershipUser("MyMembershipProvider",
                    result.Username,
                    null,
                    result.Email,
                    null,
                    null,
                    true,
                    result.Locked,
                    result.CreatedOn,
                    DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now);

                return mu;
            }
            else
            {
                throw new Exception("AdminUser not found in CustomMembershipProvider.GetUser(string username, bool userIsOnline)");
            }
        }
    }
}