﻿using AutoMapper;
using NewRepublique.AdminWebsite.Models.DTOs;
using NewRepublique.AdminWebsite.Models.ViewModels;
using NewRepublique.Data.Service;
using NewRepublique.PetaPoco.Repository.POCOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcPaging;

namespace NewRepublique.AdminWebsite.Controllers
{
    public class PosterController : Controller
    {
        private IPosterService _posterService;

        public PosterController(IPosterService posterService)
        {
            _posterService = posterService;
        }        

        [Authorize]
        public ActionResult Index(PostersViewModel model, int? page)
        {
            int totalCount = 0;
            model.PageSize = 20;

            if (page != null)
                model.PageNumber = page.Value;
            else
                model.PageNumber = 1;

            if (ModelState.IsValid)
            {
                var result = _posterService.GetPosters(model.PageNumber, model.PageSize, model.FromDate.Value, model.ToDate.Value, model.OrderBy, ref totalCount);

                model.TotalCount = totalCount;
                model.Posters = Mapper.Map<IEnumerable<Poster>, IEnumerable<PosterModel>>(result).ToPagedList(model.PageNumber, model.PageSize, totalCount);
            }


            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id, DateTime fromDate, DateTime toDate, string orderBy, int page)
        {
            PosterViewModel model = new PosterViewModel();

            var result = Mapper.Map<Poster, PosterModel>(_posterService.GetById(id));

            model.Id = result.Id;
            model.CreatedOn = result.CreatedOn;
            model.EntrantId = result.EntrantId;
            model.ImageUrl = result.ImageUrl;
            model.IsFlaggedForInappropriate = result.IsFlaggedForInappropriate;
            model.IsVisible = result.IsVisible;
            model.LastModifiedBy = result.ModifiedBy;
            model.LastModifiedOn = result.ModifiedOn;
            model.Title = result.Title;
            model.Views = result.Views;

            model.FromDate = fromDate;
            model.ToDate = toDate;
            model.OrderBy = orderBy;
            model.PageNumber = page;

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(PosterViewModel model)
        {
            if(ModelState.IsValid)
            {
                PosterModel pm = new PosterModel
                {
                    CreatedOn = model.CreatedOn,
                    EntrantId = model.EntrantId,
                    Id = model.Id,
                    ImageUrl = model.ImageUrl,
                    IsFlaggedForInappropriate = model.IsFlaggedForInappropriate,
                    IsVisible = model.IsVisible,
                    ModifiedBy = User.Identity.Name,
                    ModifiedOn = DateTime.Now,
                    Title = model.Title,
                    Views = model.Views
                };


                if (_posterService.Update(Mapper.Map<PosterModel, Poster>(pm)) > 0)
                    ViewBag.result = "Update Sucessful";
                else
                    ViewBag.result = "Update Failed";
            }

            return View(model);
        }
    }
}