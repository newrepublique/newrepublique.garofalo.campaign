﻿using MvcPaging;
using NewRepublique.AdminWebsite.Models.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NewRepublique.AdminWebsite.Models.ViewModels
{
    public class PostersViewModel
    {
        public IPagedList<PosterModel> Posters { get; set; }
        
        [Required(ErrorMessage="*")]
        public DateTime? FromDate { get; set; }

        [Required(ErrorMessage = "*")]
        public DateTime? ToDate { get; set; }

        [Required(ErrorMessage = "*")]
        public string OrderBy { get; set; }

        public int PageNumber { get; set; }

        public int PageSize { get; set; }

        public int TotalCount { get; set; }

    }
}