﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NewRepublique.AdminWebsite.Models.ViewModels
{
    public class TestModel
    {
        [Required(ErrorMessage = "Required")]
        public string Title { get; set; }
    }
}