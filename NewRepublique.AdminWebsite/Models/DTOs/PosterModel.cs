﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewRepublique.AdminWebsite.Models.DTOs
{
    public class PosterModel
    {
        public int Id { get; set; }
        public int EntrantId { get; set; }
        public string Title { get; set; }
        public string ImageUrl { get; set; }
        public int Views { get; set; }
        public bool IsFlaggedForInappropriate { get; set; }
        public bool IsVisible { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }

        public EntrantModel EntrantModel { get; set; }
    }
}