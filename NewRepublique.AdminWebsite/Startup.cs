﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NewRepublique.AdminWebsite.Startup))]
namespace NewRepublique.AdminWebsite
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
