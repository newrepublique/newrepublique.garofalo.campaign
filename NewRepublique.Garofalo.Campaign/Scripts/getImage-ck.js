/*
 *
 *  jQuery $.getImageData Plugin 0.3
 *  http://www.maxnov.com/getimagedata
 *  
 *  Written by Max Novakovic (http://www.maxnov.com/)
 *  Date: Thu Jan 13 2011
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *  
 *  Includes jQuery JSONP Core Plugin 2.4.0 (2012-08-21)
 *  https://github.com/jaubourg/jquery-jsonp
 *  Copyright 2012, Julian Aubourg
 *  Released under the MIT License.
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *  
 *  Copyright 2011, Max Novakovic
 *  Dual licensed under the MIT or GPL Version 2 licenses.
 *  http://www.maxnov.com/getimagedata/#license
 * 
 */// jQuery JSONP
(function(e){function t(){}function n(e){N=[e]}function r(e,t,n){return e&&e.apply(t.context||t,n)}function i(i){function A(e){ot++||(ut(),et&&(x[nt]={s:[e]}),q&&(e=q.apply(i,[e])),r(H,i,[e,y,i]),r(F,i,[i,y]))}function _(e){ot++||(ut(),et&&e!=b&&(x[nt]=e),r(j,i,[i,e]),r(F,i,[i,e]))}i=e.extend({},C,i);var H=i.success,j=i.error,F=i.complete,q=i.dataFilter,W=i.callbackParameter,$=i.callback,G=i.cache,et=i.pageCache,tt=i.charset,nt=i.url,rt=i.data,it=i.timeout,st,ot=0,ut=t,at,ft,lt;E&&E(function(e){e.done(H).fail(j);H=e.resolve;j=e.reject}).promise(i);i.abort=function(){!(ot++)&&ut()};if(!1===r(i.beforeSend,i,[i])||ot)return i;nt=nt||u;rt=rt?"string"==typeof rt?rt:e.param(rt,i.traditional):u;nt+=rt?(/\?/.test(nt)?"&":"?")+rt:u;W&&(nt+=(/\?/.test(nt)?"&":"?")+encodeURIComponent(W)+"=?");G||et||(nt+=(/\?/.test(nt)?"&":"?")+"_"+(new Date).getTime()+"=");nt=nt.replace(/=\?(&|$)/,"="+$+"$1");et&&(st=x[nt])?st.s?A(st.s[0]):_(st):(w[$]=n,at=e(g)[0],at.id=l+T++,tt&&(at[o]=tt),k&&11.6>k.version()?(ft=e(g)[0]).text="document.getElementById('"+at.id+"')."+h+"()":at[s]=s,L&&(at.htmlFor=at.id,at.event=c),at[p]=at[h]=at[d]=function(e){if(!at[v]||!/i/.test(at[v])){try{at[c]&&at[c]()}catch(t){}e=N;N=0;e?A(e[0]):_(a)}},at.src=nt,ut=function(e){lt&&clearTimeout(lt);at[d]=at[p]=at[h]=null;S[m](at);ft&&S[m](ft)},S[f](at,W=S.firstChild),ft&&S[f](ft,W),lt=0<it&&setTimeout(function(){_(b)},it));return i}var s="async",o="charset",u="",a="error",f="insertBefore",l="_jqjsp",c="onclick",h="on"+a,p="onload",d="onreadystatechange",v="readyState",m="removeChild",g="<script>",y="success",b="timeout",w=window,E=e.Deferred,S=e("head")[0]||document.documentElement,x={},T=0,N,C={callback:l,url:location.href},k=w.opera,L=!!e("<div>").html("<!--[if IE]><i><![endif]-->").find("i").length;i.setup=function(t){e.extend(C,t)};e.jsonp=i})(jQuery);(function(e){e.getImageData=function(t){var n=/(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;if(t.url){var r=location.protocol==="https:",i="";t.server&&n.test(t.server)&&(!r||t.server.indexOf("http:")!=0)?i=t.server:i="//img-to-json.appspot.com/";i+="?callback=?";e.jsonp({url:i,data:{url:escape(t.url)},dataType:"jsonp",timeout:t.timeout||1e4,success:function(n,r){var i=new Image;e(i).load(function(){this.width=n.width;this.height=n.height;typeof t.success==typeof Function&&t.success(this)}).attr("src",n.data)},error:function(e,n){typeof t.error==typeof Function&&t.error(e,n)}})}else typeof t.error==typeof Function&&t.error(null,"no_url")}})(jQuery);