$(document).ready(function(){
  PointerEventsPolyfill.initialize({});
}); 

function open404(){
  $('.error-modal-box').fadeIn(300);
}

function close404(){
  $('.error-modal-box').fadeOut(300);
}

$('.close-error').click(function(){
  close404()
})

$(window).load(function() {
  $('.homeslider').flexslider({
    animation: "slide",
    itemWidth: 290,
    directionNav: true,
    controlNav: true,
    prevText: "",
    nextText: "",
    slideshowSpeed: 3500
  });
});

function facebookLogOut(){
  FB.logout(function(response) {
  window.location.href = '/';
  });
};


// sorter
$('.sortOptions a').click(function(){
  $('.sortOptions .active').removeClass('active');
  $(this).addClass('active');
})

// login stuff
function loginToFB(){
   // call facebook and check whether the user has logged in or not
  FB.getLoginStatus(function(response) {
      loginResponse(response);
    });
}

$('.fbLogin').click(function(e){
  e.preventDefault;
  loginToFB()
});

function finalShare(){
  FB.ui({
    method: 'share',
    href: 'https://developers.facebook.com/docs/',
  }, function(response){});
}

function loginResponse(response) { 
    if (response.status === 'connected') {
      window.location.href = '/inspiration';
    } else if (response.status === 'not_authorized') {
      FB.login(function(response){
        if (response.authResponse) {
           window.location.href = '/inspiration';
         } else {
          // to do: what if user cancels?
           window.location.href = '/';
           console.log('User cancelled login or did not fully authorize.');
         }
      }, {scope: 'email,user_photos'});
    } else {
      FB.login(function(response){
        if (response.authResponse) {
           window.location.href = '/inspiration';
         }
      }, {scope: 'email,user_photos'});  
    }
  }




  // security check
function securityCheck(){
   // call facebook and check whether the user has logged in or not
  FB.getLoginStatus(function(response) {
      securityResponse(response);
    });
}

function securityResponse(response) { 
    if (response.status === 'connected') {
      return;
    } else {
      window.location.href = '/';
    };
  }


function openModal(){
  $('.gallery-lightbox').fadeIn(300);
}

function closeModal(){
  $('.gallery-lightbox').fadeOut(300);
}

  // modal window in gallery
  $('.gallery-item').click(function(){
    openModal()
  })
  $('.image-contain .close').click(function(){
    closeModal()
  })


$('.the-image').mouseenter(function(){
  $('.poster-details').fadeIn(300);
});
$('.the-image').mouseleave(function(){
  $('.poster-details').fadeOut(300);
});