  //// EVERYTHING WE NEED FOR THE FACEBOOK API //
var userID;
var myName;
  // Handle different log in possibilities 
  function statusChangeCallback(response) {
    
    if (response.status === 'connected') {
      // If everything is ok 
      FB.api('/me', function(response) {
        myName = response.name; 
        userID = response.id; 
        $('.myName').html(myName)
        $('.loginCheck .fbStatus').fadeIn(300)
        $('.loading').fadeOut(300);
      });   
    

    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      
    }
  }



  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below. 
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }



  // initialise the fb api and get login state
  window.fbAsyncInit = function() {
    FB.init({
        appId: '628771203868243',
      cookie     : true,  // enable cookies to allow the server to access 
                          // the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v2.0' // use version 2.0
    });

    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));




  