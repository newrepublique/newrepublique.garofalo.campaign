// EVERYTHING WE NEED FOR THE EDITOR //

// global variables
var currentFace; // face we're working with e.g. face-1
var theDestination; // the image we're replacing
var facePosition; // top and left co-ordinates of current face
var currentScale; // scale of the inserted image
var webcamRunning; // state of the webcam
var finalURL; // final encoded string for backend
var canvasNumber; // current canvas we're working on
var posterTitle; // title of poster for backend
var faceCount = 0; // count of posters

// global oh-dear-wtf-do-i-do function
function shutDownEVERYTHING() {
    $('.webcam-contain').fadeOut(300);
    $('.takePhoto').fadeOut(300);
    $('.blackOut').fadeOut(300);
    $('.clickThrough').removeClass('clickThrough');
    $('.activeHotspot').removeClass('activeHotspot');
    $('.metaIcon.active').removeClass('active')
    $('.hotspot').fadeIn(300);
    $('.hotspot-container').fadeIn(300);
    $('.removable .hotspot').hide();
    $('.pickInput').show();
    $('.currentSection').text('Insert an Image');
    // face in the back button
    $('.sidebar-back').hide();
    // call for FB albums
    $('.photoPicker').hide();
    $('.removable .hotspot').hide();
    currentScale = 1.0;
    webcamRunning == false
}

// utility function to check number of faces added
function countFaces() {
    faceCount = 0
    $('canvas').each(function () {
        faceCount = faceCount + 1
    });
};

// function to check if user has added at least one face
// used to control visibility of Save and Continue button
function checkIfChanged() {
    countFaces()
    if (faceCount == 0) {
        $('.savePoster').fadeOut(300);
    } else {
        $('.savePoster').fadeIn(300);
    }
}

//// BOUNCING OUT POSTERS ////
// function to render out the final poster we'll send to the backend


// Save and Continue Button
$('.savePoster').click(function () {
    // start loading, then after 3 seconds start bouncing out the poster
    // we add this delay so that the loading animation doesnt stutter when it fades in
    //$('.loading').fadeIn(300);
    newMessage();
    //setTimeout(function(){
    bouncePoster();
    posterTitle = $('#text-editor-input').val();
    //}, 0);
});



// We need to do 2 passes to render the poster out as an image

// First pass: remove all the editor-related markup and leave only <canvas> elements, the poster, and the edited text. In this pass we compensate for some positioning issues that arise due to the user scaling a face using CSS3 transforms, which are not recognised by html2canvas plugin. We also have to copy over the contents of each canvas manually, because jQuery's .clone() doesn't bring canvas data over.

// Second pass: We convert all that to one final <canvas>, which can be accurately converted to a final image/

// This function initialises both passes
// however for reference, bouncePoster is our first pass and renderPoster is our second
var maskStyle;
function bouncePoster() {

    // duplicate poster into a container we set up for the first pass
    $('.workSpace').clone().appendTo('.renderDump');

    // remove stuff we don't need
    $('.renderDump .hotspot-container').remove();
    $('.renderDump .controls').remove();
    $('.renderDump .title-hotspot').remove();
    $('.renderDump .cast-hotspot').remove();

    // loop through the duplicated canvases
    $('.renderDump canvas').each(function () {

        // figure out which canvas we're on
        // we use this a few times in this function
        var currentCanvas = $(this).parent().attr('id');

        // get CSS properties of the mask container for use later
        maskStyle = $(this).parent().attr('style');

        // get height and width of canvas
        var canvasWidth = $(this).width()
        var canvasHeight = $(this).height()

        // Create a new canvas with the correct dimensions
        // and then duplicate it into the new poster
        var newCanvasName = currentCanvas + '-target';
        $('<canvas>').attr({
            id: newCanvasName
        }).appendTo('.renderDump .workSpace');

        // select the new canvas
        var newCanvas = document.getElementById(newCanvasName);

        // define source canvas to get our image data from (OHMYGAWWWDDD)
        var canvasNumber = currentCanvas.replace("canvas", "");
        var sourceCanvas = $('canvas.face-' + canvasNumber).get(0);

        // remove more unneeded stuff
        $('.renderDump .face-canvas.face-' + canvasNumber).remove();
        $('.face#face' + canvasNumber).remove();

        // get the current canvas's css properties
        var currentStyles = $(this).attr('style')

        // wrap new canvas in a container so we can mask it
        $('#' + newCanvasName).wrap("<div class='face-canvas face-" + canvasNumber + "' id='canvas" + canvasNumber + "'></div>");

        // I'll be honest... I forget why this is here. but it breaks without it so #yolo
        var currentScale = $(this).attr('data-scale');

        $('#' + newCanvasName).attr('data-scale', currentScale);

        // z-index the new canvas so it doesnt act cheeky and hop on top of the poster
        // when we render it
        $('.face-canvas').attr('style', 'z-index:0!important')

        // set the width and height of the new canvas
        newCanvas.width = canvasWidth
        newCanvas.height = canvasHeight

        // copy over the existing styles
        $('#' + newCanvasName).attr('style', currentStyles);

        // apply new css rules
        $('#' + newCanvasName).css({
            'z-index': '0!important',
            'position': 'relative',
            'transform': 'none'
        });

        // draw the new canvas into new canvas from the old one
        // and also scale it so its the right size, according to how much the user scaled the 
        // image during placement.
        var destCtx = newCanvas.getContext('2d');
        destCtx.scale(currentScale, currentScale);
        destCtx.drawImage(sourceCanvas, 0, 0);
    });

    // move on to the next pass
    renderPoster()
};

function postPosterToApi(encodedImg) {
    $('.button.large.savePoster').hide();
    $('.loading').fadeIn(300);

    if ($('.workSpace').attr('id') == 'tuscan') {
        if (posterTitle != 'Tuscan Adventure') {
            posterTitle = posterTitle + ' Adventure';
        }
    }

    $.post("/api/Picture/Post", {
        author: myName,
        userid: userID,
        poster: finalURL,
        title: posterTitle
    })
    .always(function (data) {
        window.location.href = '/Poster/Final/' + data;
    })
}

// Our second and final pass of the render process
function renderPoster() {
    // use html2canvas plugin to target the dumped out poster
    html2canvas($('.renderDump'), {
        onrendered: function (canvas) {
            // add the final rendered canvas and give it an ID
            document.body.appendChild(canvas);
            $('canvas:last').attr('id', 'finalCanvas')

            // target the final canvas and convert it so an encoded string
            $('#finalCanvas').get(0)
            finalURL = canvas.toDataURL("image/jpg");

            postPosterToApi(finalURL);

            // remove the divs we used for the multi-pass render
            $('.renderDump, .finalDump').remove()

            // remove the canvas once we've extracted the string we need
            $('#finalCanvas').remove()
        }
    });

    // NOTE FOR LEM:
    // comment this out when you have a URL to post to 
    $('.loading').fadeOut(300);
}


//// EDITING TITLES ////

// check which poster we're editing
var currentTemplate = $('.workSpace').attr('id');

// utility variables
var currentText;
var currentFirst;
var currentLast;
var currentCast;
var allowedTitleChars;
var allowedFirstChars;
var allowedLastChars;
var allowEdit = true;

// set the max amount of characters depending on the template
if (currentTemplate == 'film-noir') {
    allowedTitleChars = 21;
    allowedFirstChars = 10;
    allowedLastChars = 10;
}
if (currentTemplate == 'tuscan') {
    allowedTitleChars = 10;
    allowedFirstChars = 10;
    allowedLastChars = 10;
}
if (currentTemplate == 'empire') {
    allowedTitleChars = 10;
}
if (currentTemplate == 'fantasy') {
    allowedTitleChars = 17;
}
if (currentTemplate == 'romance') {
    allowedTitleChars = 12;
    allowedFirstChars = 8;
    allowedLastChars = 8;
}
if (currentTemplate == 'romcom') {
    allowedTitleChars = 14;
    allowedFirstChars = 7;
    allowedLastChars = 7;
}

// update the title "characters remaining" text
function updateTitleCountdown() {
    // work out how many characters are remaining and update the text
    var remaining = allowedTitleChars - $('#text-editor-input').val().length;
    $('.charRemaining').text(remaining + ' characters remaining.');

    // check if user has exceed character limit
    if (remaining < 0) {
        $('.charRemaining').addClass('over-limit');
        $('.update-title').addClass('disabled');
        allowEdit = false;
    } else {
        $('.charRemaining').removeClass('over-limit');
        $('.update-title').removeClass('disabled');
        allowEdit = true;
    };
};

// update the first name "characters remaining" text
function updateFirstCountdown() {
    // work out how many characters are remaining and update the text
    var remaining = allowedFirstChars - $('#first-name-input').val().length;
    $('.firstRemaining').text(remaining + ' characters remaining.');

    // check if user has exceed character limit
    if (remaining < 0) {
        $('.firstRemaining').addClass('over-limit');
        $('.update-cast').addClass('disabled');
        allowEdit = false;
    } else {
        $('.firstRemaining').removeClass('over-limit');
        $('.update-cast').removeClass('disabled');
        allowEdit = true;
    };
};

// update the last name "characters remaining" text
function updateLastCountdown() {
    // work out how many characters are remaining and update the text
    var remaining = allowedLastChars - $('#last-name-input').val().length;
    $('.lastRemaining').text(remaining + ' characters remaining.');

    // check if user has exceed character limit
    if (remaining < 0) {
        $('.lastRemaining').addClass('over-limit');
        $('.update-cast').addClass('disabled');
        allowEdit = false;
    } else {
        $('.lastRemaining').removeClass('over-limit');
        $('.update-cast').removeClass('disabled');
        allowEdit = true;
    };
};

updateFirstCountdown();
updateLastCountdown();
updateTitleCountdown();

// update countdown whenever text is entered
$('#text-editor-input').change(updateTitleCountdown);
$('#text-editor-input').keyup(updateTitleCountdown);
$('#first-name-input').change(updateFirstCountdown);
$('#first-name-input').keyup(updateFirstCountdown);
$('#last-name-input').change(updateLastCountdown);
$('#last-name-input').keyup(updateLastCountdown);

// open and close the editor
function openTextEdit() {
    $('.title-editor').fadeIn(300);
    $('#text-editor-input').val(currentText)
    $('#text-editor-input').focus();
    updateTitleCountdown();
};
function closeTextEdit() {
    $('.title-editor').fadeOut(300);
}

// grab the current title when clicked
$('.title-hotspot').click(function () {
    // grab the current title
    currentText = $(this).next().children().text();
    editedText = $(this).next().children();
    openTextEdit();
});

// save and cancel buttons within the editor
$('.update-title').click(function (e) {
    e.preventDefault;
    if (allowEdit == true) {
        currentText = $('#text-editor-input').val();
        $(editedText).text(currentText);
        closeTextEdit();
    } else {
        return;
    }
});
$('.update-cast').click(function () {
    if (allowEdit == true) {
        currentFirst = $('#first-name-input').val();
        currentLast = $('#last-name-input').val();
        $('.' + currentCast + ' .first-name').text(currentFirst)
        $('.' + currentCast + ' .last-name').text(currentLast)
        closeCastEdit()
    } else {
        return;
    }
});
$('.cancel-title').click(function () {
    $(editedText).text(currentText);
    closeTextEdit();
});

$('.cast-hotspot').click(function () {
    currentFirst = $(this).next().children('.first-name').text();
    currentLast = $(this).next().children('.last-name').text();
    currentCast = $(this).attr('data-cast');
    openCastEdit()
});

$('.cancel-cast').click(function () {
    closeCastEdit()
});

function closeCastEdit() {
    $('.cast-editor').fadeOut(300);
};

function openCastEdit() {
    $('#first-name-input').val(currentFirst)
    $('#last-name-input').val(currentLast)
    $('.cast-editor').fadeIn(300);
    updateFirstCountdown();
    updateLastCountdown();
};

///// END EDITING TITLES /////

///// WEBCAM STUFF /////
$('.closeWebcam').click(function () {
    shutDownEVERYTHING()
    $('body').css('overflow', 'auto');
});

$('.webcam-input').click(function () {
    // get position of current face
    facePosition = $('.face-canvas.' + currentFace).position();

    // call and position the webcam behind the current face
    $("#webcam").scriptcam({
        //flip: 1,
        showMicrophoneErrors: false,
        width: 640,
        height: 480,
        showDebug: true,
        disableHardwareAcceleration: 1
        //onError: shutDownEVERYTHING
    });

    webcamRunning = true

    if (webcamRunning == true) {
        $('body').keyup(function (e) {
            if (e.keyCode == 32) {
                //$('.flash').fadeIn(100).fadeOut(100);
                base64_toimage()
            };
        });
    };

    $('.sidebar').toggle("slide", 300)
    $('.webcam-contain').fadeIn(300);
    $('body').css('overflow', 'hidden');
    $('.takePhoto').fadeIn(300);
})

// functions for the webcam (note to self: not sure if we need this first one)
function base64_tofield() {
    $('#formfield').val($.scriptcam.getFrameAsBase64());
};

// capture as an image
function base64_toimage() {
    $('body').css('overflow', 'auto');
    theDestination = '.face-canvas.' + currentFace + ' img'

    // need to target current face
    $(theDestination).attr("src", "data:image/png;base64," + $.scriptcam.getFrameAsBase64());

    $('.controls').show();
    $('.face-canvas.' + currentFace).insertAfter($(".controls"));
    // current hotspot
    var currentHotspot = $('div[data-id="' + currentFace + '"]');
    //var hotspotPosition = $(currentHotspot)[0].getBoundingClientRect()
    var hotspotLeft = $(currentHotspot).css('left').replace('px', '');
    var hotspotTop = $(currentHotspot).css('top').replace('px', '');
    var hotspotHeight = $(currentHotspot).css('height').replace('px', '');
    var hotspotWidth = $(currentHotspot).css('width');

    $('.controls').css({
        "top": parseInt(hotspotTop) + parseInt(hotspotHeight) + 'px',
        "left": hotspotLeft + 'px'
    });

    // run the image effect via caman
    $('.activeFace img').fadeOut(300);
    $('.' + currentFace + '-original').fadeOut(300);
    $(theDestination).draggable();

    $('.activeHotspot').addClass('has-face')
    $('.webcam-contain').fadeOut(300);
    checkIfChanged()
};

// generic function to empty out a div
function clearOut(destination) {
    $(destination).children().fadeOut(500);
    $(destination).empty();
}

// get tagged photos of a user
function getFBPictures(destination) {
    // start the loading animation and empty the sidebar
    $('.sidebarLoading').fadeIn(300);
    clearOut(destination);

    // make a "i need tagged photos" call to the Facebook api
    FB.api('/me/photos', function (response) {
        var arrayLength = response.data.length;
        // loop through the response and output markup template
        for (var i = 0; i < arrayLength; i++) {
            $(destination).append('<li><a class="th"><img src="' + response.data[i].source + '" class="singleImage"></a></li>');
        }
        // finish the loading animation
        $('.sidebarLoading').fadeOut(300);
    });
};

// get user's photo albums
function getFBAlbums(destination) {
    // start the loading animation and empty the sidebar
    $('.sidebarLoading').fadeIn(300);
    clearOut(destination);

    // grab ourselves an FB access token 
    // so we can refer to each of the user's photo albums
    var accessToken;
    FB.getLoginStatus(function (response) {
        if (response.status === 'connected') {
            accessToken = response.authResponse.accessToken
        }
    });
    // call the facebook api and grab each of the user's photo albums
    // then spit them out to the detination
    FB.api('/me/albums', function (response) {
        var arrayLength = response.data.length;
        for (var i = 0; i < arrayLength; i++) {
            $(destination).append('<li class="albumCover" data-albumId="' + response.data[i].id + '"><div class="albumContainer"><img src="https://graph.facebook.com/' + response.data[i].id + '/picture?access_token=' + accessToken + '" class="albumImage"></div>' + '<p class="albumTitle">' + response.data[i].name + '</p></li>');
        }
        // end the loading animation
        $('.sidebarLoading').fadeOut(300);
    });
};

$(document).on("click", ".backToAlbums", function () {
    $(this).fadeOut(300)
});

$(document).on("click", ".albumCover", function () {
    var currentAlbum = $(this).attr('data-albumId');
    getAlbumPictures('.photoPicker', currentAlbum);
});

function getAlbumPictures(destination, albumId) {
    $('.sidebarLoading').fadeIn(300);
    clearOut(destination);
    $('<a class="backToAlbums"> << Back to Albums</a>').insertAfter('.sidebarTitle')
    albumPath = '/' + albumId + '/photos'

    FB.api(albumPath, function (response) {
        var arrayLength = response.data.length;
        console.log(arrayLength)
        // loop through the response and output markup template
        for (var i = 0; i < arrayLength; i++) {
            $(destination).append('<li><a class="th"><img src="' + response.data[i].source + '" class="singleImage"></a></li>');
        }
        // finish the loading animation
        $('.sidebarLoading').fadeOut(300);
    });
}

$(document).on("click", ".backToAlbums", function () {
    getFBAlbums('.photoPicker');
});

var currentScale = 1.0;
var localCurrentScale = 1.0;
// when user changes the scale slider, resize the image via CSS
$('#image-scale').change(function () {
    currentScale = $(this).val();
    $(theDestination).css({
        'transform': 'scale(' + currentScale + ')',
        '-webkit-transform': 'scale(' + currentScale + ')',
        '-moz-transform': 'scale(' + currentScale + ')'
    });
    $(theDestination).attr('data-scale', currentScale);
});

$('.minus-scale').click(function (e) {
    e.preventDefault();
    
        currentScale = currentScale - 0.04;
        console.log(currentScale);

        $(theDestination).css({
            'transform': 'scale(' + currentScale + ')',
            '-webkit-transform': 'scale(' + currentScale + ')',
            '-moz-transform': 'scale(' + currentScale + ')'
        });

        $(theDestination).attr('data-scale', currentScale);

    
})

$('.add-scale').click(function (e) {
    e.preventDefault()

    
        currentScale = currentScale + 0.04
        
        console.log(currentScale)
        $(theDestination).css({
            'transform': 'scale(' + currentScale + ')',
            '-webkit-transform': 'scale(' + currentScale + ')',
            '-moz-transform': 'scale(' + currentScale + ')'
        });
        $(theDestination).attr('data-scale', currentScale);
    

    
})


// Editing a face is enabled
function enableEdit() {
    $("#face").draggable().css("position", "absolute");;
    $('.controls').fadeIn(300)
};

// Editing a face is disabled
function disableEdit() {
    $("#face").draggable('disable');
    $('.controls').fadeOut(300);
    closeSidebar()
};

var realPosition;
var newPosition;
var thisFace;
var magicLeft;
var magicRight;
var currentLeft;
var strippedLeft;
var currentTop;
var strippedTop;

function addEffect(image) {
    $('body').css('overflow', 'auto');
    $('.loading').fadeIn(300);
    newMessage()

    realPosition = $(image)[0].getBoundingClientRect()
    console.log(realPosition.left);

    var imageHeight = $(image).height();
    var imageWidth = $(image).width();
    var adjustedHeight = imageHeight * currentScale;
    var adjustedWidth = imageWidth * currentScale;
    $(image).width(adjustedWidth);
    $(image).height(adjustedHeight);

    $(image).css({
        'transform': 'none'
    });

    $("#face").draggable('disable');
    $('.marker').hide();
    $('.activeHotspot').addClass('removable');
    $('.activeHotspot .metaIcon').attr('id', 'cross')

    if (currentTemplate == 'film-noir') {
        Caman(image, function () {
            this.exposure(50)
            this.contrast(30)
            this.greyscale()
            this.colorize("#e0d3b2", 20)
            this.exposure(-20)
            this.noise(4)
            this.render()
        });
    }

    if (currentTemplate == 'tuscan') {
        Caman(image, function () {
            this.colorize('#e8ab8c', 30);
            this.exposure(15);
            this.contrast(18);
            this.render();
        });
    }

    if (currentTemplate == 'empire') {
        Caman(image, function () {
            this.contrast(5)
            this.colorize('#e6b796', 5)
            this.render()
        });
    }

    if (currentTemplate == 'fantasy') {
        Caman(image, function () {
            this.colorize("#eec38d", 40)
            this.contrast(25);
            this.render()
        });
    }

    if (currentTemplate == 'romance') {
        Caman(image, function () {
            this.contrast(20)
            this.saturation(-30)
            this.colorize('#b0a096', 20)
            this.render()
        });
    }

    if (currentTemplate == 'romcom') {
        Caman(image, function () {
            this.colorize("#f3bcad", 20)
            this.contrast(15);
            this.saturation(-20);
            this.render()
        });
    }

    newPosition = $(image)[0].getBoundingClientRect();

    thisFace = currentFace
    magicLeft = realPosition.left - newPosition.left
    magicTop = realPosition.top - newPosition.top
};



// when rendering is finished
Caman.Event.listen("renderFinished", function () {


    currentLeft = $('canvas.' + thisFace).css('left')
    strippedLeft = currentLeft.replace('px', '')

    currentTop = $('canvas.' + thisFace).css('top')
    strippedTop = currentTop.replace('px', '')


    var finalLeft = parseInt(strippedLeft) + parseInt(magicLeft) + 'px'
    var finalTop = parseInt(strippedTop) + parseInt(magicTop) + 'px'
    console.log(finalLeft)
    $('canvas.' + thisFace).css('left', finalLeft)
    $('canvas.' + thisFace).css('top', finalTop)

    $('.controls').fadeOut(300);
    $('.loading').fadeOut(300);
    $('#face').fadeIn(300);
    closeSidebar();
    editModeOn = false;
    checkIfChanged()
});


$('.facebook-input').click(function () {
    // fadeout the fb/webcam icons
    $('.pickInput').fadeOut(300);
    $('.currentSection').text('Choose from Facebook');
    // face in the back button
    $('.sidebar-back').fadeIn(300);
    // call for FB albums
    $('.photoPicker').fadeIn(300);
    getFBAlbums('.photoPicker');
});

$('.sidebar-back').click(function () {
    // fade out the picker if visible
    $('.photoPicker').fadeOut(300)
    $('.currentSection').text('Insert an Image');
    // fade out button
    $(this).fadeOut(300);
    // show input picker
    $('.pickInput').fadeIn(300);
});


function resetWorkspace() {
    currentFace = '';
    theDestination = '';
    facePosition = '';
    $('.backToAlbums').remove();
    $('.activeFace').removeClass('activeFace');
    editModeOn = false
};

// open sidebar function
function openSidebar() {
    $('.sidebar').toggle("slide", 300)
    //getFBAlbums('.photoPicker')
    $('.blackOut').fadeIn(300);
    $('.hotspot').fadeIn(300);
    $('.hotspot-container').fadeIn(300);
}

// close sidebar function
function closeSidebar() {
    $('.sidebar').hide("slide", 300)
    $('.blackOut').fadeOut(300);
    $('.clickThrough').removeClass('clickThrough');
    $('.activeHotspot').removeClass('activeHotspot');
    $('.metaIcon.active').removeClass('active')
    $('.hotspot').fadeIn(300);
    $('.hotspot-container').fadeIn(300);
    $('.removable .hotspot').hide();
    $('.pickInput').show();
    $('.currentSection').text('Insert an Image');

    // face in the back button
    $('.sidebar-back').hide();
    // call for FB albums
    $('.photoPicker').hide();
    $('.removable .hotspot').hide();
    currentScale = 1.0;
    resetWorkspace();
};

$('.closeSidebar').click(function () {
    $('.' + currentFace + '-original').fadeIn(300);
    $('.controls').fadeOut(300)
})

// bind to a button
$('.closeSidebar').click(function () {
    closeSidebar()
});

// When user clicks on a facebook photo
// note to self: Since Albums AND Single images are loaded
// into "photoPicker", you'll need to rejig this code
// since it grabs the src attr of the clicked image and puts it into
// the canvas. 


$(document).on("click", ".photoPicker .singleImage", function () {
    // start loading animation
    //$('.loading').fadeIn(300); 
    // get the current image by src attribute
    $('.activePic').removeClass('activePic')
    $(this).parent().parent().addClass('activePic')
    theImage = $(this).attr('src');
    theDestination = '.face-canvas.' + currentFace + ' img'
    // we need to use getImageData because using an externally hosted 
    // image inside a <canvas> gives security error

    // essentially this sends the image to a node.js server and returns
    // an encoded version we can use within a <canvas>.
    $('.loading').fadeIn(300);
    newMessage()
    $.getImageData({

        url: theImage,
        // php server to send back image as encoded string
        server: "http://newrepublique.com/imagedata/",
        timeout: 20000,

        // if it works
        success: function (image) {

            // hide loading
            $('.loading').fadeOut(300);
            // get server response and use its src attribute as the new working image 
            theImage = image.src

            // send the new image src to the working canvas
            $(theDestination).attr('src', theImage)
            $('.controls').show();
            $('.face-canvas.' + currentFace).insertAfter($(".controls"));

            // current hotspot
            var currentHotspot = $('div[data-id="' + currentFace + '"]');
            //var hotspotPosition = $(currentHotspot)[0].getBoundingClientRect()
            var hotspotLeft = $(currentHotspot).css('left').replace('px', '');
            var hotspotTop = $(currentHotspot).css('top').replace('px', '');
            var hotspotHeight = $(currentHotspot).css('height').replace('px', '');
            var hotspotWidth = $(currentHotspot).css('width');

            $('.controls').css({
                "top": parseInt(hotspotTop) + parseInt(hotspotHeight) + 'px',
                "left": hotspotLeft + 'px'
            })
            // run the image effect via caman
            $('.activeFace img').fadeOut(300);
            $('.' + currentFace + '-original').fadeOut(300);
            $(theDestination).draggable()

            $('.activeHotspot').addClass('has-face')
        },
        // TO DO: What happens if the server doesn't return an image?
        error: function (xhr, text_status) {
            console.log(xhr, text_status)
            alert('Server timed out. Sorry about that. Please refresh and try again.')
        }
    });
});





// removing a face
$(document).on("click", "#cross", function () {
    currentFace = $(this).parent().attr('data-id')
    $('canvas.' + currentFace).fadeOut(300);
    $('canvas.' + currentFace).remove();

    $('.face-canvas.' + currentFace).append('<img src="" class="' + currentFace + '">')

    $('img.' + currentFace).attr('src', '');
    $('img.' + currentFace).fadeIn(300);
    $('.' + currentFace + '-original').fadeIn(300);
    $(this).attr('id', 'camera')
    $(this).parent().children('.hotspot').fadeIn(300);
    $(this).parent().removeClass('removable')
    checkIfChanged()
})

// new hotspot model
$('.hotspot:not(.hotspot.has-face)').click(function () {
    // set face-n as currentface
    currentFace = $(this).parent().attr('data-id');
    openSidebar();

    // hide other hotspots
    $('.activeHotspot').removeClass('activeHotspot')
    $(this).parent().addClass('activeHotspot');
    $('.hotspot-container:not(.activeHotspot)').fadeOut(300);

    // & set active icon states
    $(this).parent().children('.metaIcon').addClass('active');

    // add class that lets user click through the hotspot
    $('.clickThrough').removeClass('clickThrough');
    $(this).addClass('clickThrough');
    $(this).parent().addClass('clickThrough');
});

// terms of use modal
$('.terms-of-use').click(function () {
    $('.terms-container').fadeIn(300);
})
$('.close-terms').click(function () {
    $('.terms-container').fadeOut(300);
})

// abandon page modal controls
$('.backToInspiration').click(function () {
    $('.abandon-container').fadeIn(300);
});

$('.close-abandon').click(function () {
    $('.abandon-container').fadeOut(300);
});


// loading messages
var loaders = new Array("Fear and loading in Las Vegas...", "Say hello to my loading friend...", "E.T. load home...", "There's no place like load...", "Houston, we have a loading...", "You can't handle the load...", "I've got a feeling we're not in loading anymore...", "Love the smell of loading in the morning...", "Frankly my dear, I don't give a load...", "Load it again, Sam...", "Mama always said life is like loading...", "May the load be with you...", "Go ahead: load my day...", "They can take our lives but they'll never take our loading...", "I can't load the ring, Mr Frodo. But I can load you.", "A load time ago in a galaxy far, far away...", "Here's loading at you, kid.", "Load of the Rings", "Popping popcorn...");
var numberOfLoaders = loaders.length;
var number = 1 + Math.floor(Math.random() * numberOfLoaders);
$('.loadingMessage').text(loaders[number]);

function newMessage() {
    var number = 1 + Math.floor(Math.random() * numberOfLoaders);
    $('.loadingMessage').text(loaders[number]);
}




// obligatory BoC reference
console.log('In a beautiful place out in the country.');


