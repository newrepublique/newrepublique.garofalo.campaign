﻿using System.Web;
using System.Web.Optimization;

namespace NewRepublique.Garofalo.Campaign
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/webfonts.css",
                      "~/Content/Site.css",
                      "~/Content/animate.css",
                      "~/Content/spinner.css",
                      "~/Content/flexslider.css"));

            //bundles.Add(new StyleBundle("~/Content/simplemodalcss").Include(
            //        "~/Content/SimpleModal/basic.css",
            //        "~/Content/SimpleModal/basic_ie.css",
            //        "~/Content/SimpleModal/demo.css"
            //    ));
        }
    }
}
