using Microsoft.Practices.Unity;
using NewRepublique.BusinessLogic;
using NewRepublique.Data.Service;
using NewRepublique.Infrastructure.Helpers;
using NewRepublique.PetaPoco.Repository.Business;
using NewRepublique.PetaPoco.Repository.Business.Interface;
using System.Web.Http;
using Unity.WebApi;

namespace NewRepublique.Garofalo.Campaign
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            
            // register all your components with the container here
            // it is NOT necessary to register your controllers
            
            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<IRepository, PetaPocoRepository>();
            container.RegisterType<IPosterService, PosterService>();
            container.RegisterType<IPosterLogic, PosterLogic>();
            container.RegisterType<IEmailHelper, EmailHelper>();
            
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}