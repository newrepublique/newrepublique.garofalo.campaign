﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace NewRepublique.Garofalo.Campaign
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               name: null,
               url: "Inspiration",
               defaults: new { controller = "Poster", action = "Inspiration" }
           );

            routes.MapRoute(
               name: null,
               url: "Error404",
               defaults: new { controller = "Home", action = "Error404" }
           );

            routes.MapRoute(
               name: null,
               url: "Error500",
               defaults: new { controller = "Home", action = "Error500" }
           );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
