using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc4;
using NewRepublique.PetaPoco.Repository.Business.Interface;
using NewRepublique.PetaPoco.Repository.Business;
using NewRepublique.BusinessLogic;
using NewRepublique.Data.Service;
using AutoMapper;
using NewRepublique.PetaPoco.Repository.POCOs;
using NewRepublique.Garofalo.Campaign.Models.DTOs;
using NewRepublique.Infrastructure.Helpers;

namespace NewRepublique.Garofalo.Campaign
{
    public static class Bootstrapper
    {
        public static IUnityContainer Initialise()
        {
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));

            return container;
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();    
            RegisterTypes(container);
            container.RegisterType<IRepository, PetaPocoRepository>();
            container.RegisterType<IPosterService, PosterService>();
            container.RegisterType<IPosterLogic, PosterLogic>();
            container.RegisterType<IEmailHelper, EmailHelper>();

            return container;
        }

        public static void RegisterTypes(IUnityContainer container)
        {

        }
        public static void RegisterMappings()
        {
            Mapper.CreateMap<Entrant, EntrantModel>();
            Mapper.CreateMap<EntrantModel, Entrant>();

            Mapper.CreateMap<Poster, PosterModel>();
            Mapper.CreateMap<PosterModel, Poster>();

            Mapper.CreateMap<PosterModalModel, PosterModalCreatedOn>();
            Mapper.CreateMap<PosterModalCreatedOn, PosterModalModel>();

            Mapper.CreateMap<PosterModalViews, PosterModalModel>();
            Mapper.CreateMap<PosterModalModel, PosterModalViews>();
        }
    }
}