﻿using AutoMapper;
using NewRepublique.Data.Service;
using NewRepublique.Garofalo.Campaign.Models.DTOs;
using NewRepublique.Garofalo.Campaign.Models.ViewModels;
using NewRepublique.PetaPoco.Repository.POCOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NewRepublique.Garofalo.Campaign.Controllers
{
    public class PosterController : Controller
    {
        private IPosterService _posterService;

        public PosterController(IPosterService posterService)
        {
            _posterService = posterService;
        }

        public ActionResult Inspiration()
        {
            return View();
        }

        public ActionResult Filmnoir()
        {
            return View();
        }

        public ActionResult Tuscan()
        {
            return View();
        }

        public ActionResult Empire()
        {
            return View();
        }

        public ActionResult Romance()
        {
            return View();
        }

        public ActionResult RomCom()
        {
            return View();
        }

        public ActionResult Fantasy()
        {
            return View();
        }

        public ActionResult Final(string id)
        {
            var result = Mapper.Map<Poster, PosterModel>(_posterService.GetById(int.Parse(id)));

            ViewBag.imageurlfromcdn = result.ImageUrl;
            ViewBag.posterid = result.Id;
            ViewBag.thumbnail = result.ThumbnailUrl;
            ViewBag.posterurlforfb = string.Format("{0}://{1}/Gallery/ShowPoster?id={2}&autoDisplayModal=true", Request.Url.Scheme, Request.Url.Authority, result.Id);

            return View();
        }
	}
}