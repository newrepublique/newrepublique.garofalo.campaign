﻿using NewRepublique.Data.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NewRepublique.Garofalo.Campaign.Controllers
{
    public class UpdatePictureController : ApiController
    {
        private IPosterService _posterService;

        public UpdatePictureController(IPosterService posterService)
        {
            _posterService = posterService;
        }
        public void Post(string id)
        {
            HttpRequestMessage request = this.Request;
            var result = request.Content.ReadAsStringAsync().Result;

            int posterId = int.Parse(result.Substring(3));
            var poster = _posterService.GetById(posterId);
            poster.Share = poster.Share + 1;
            _posterService.Update(poster);

        }
    }
}
