﻿using AutoMapper;
using NewRepublique.BusinessLogic;
using NewRepublique.Data.Service;
using NewRepublique.Garofalo.Campaign.Models.DTOs;
using NewRepublique.Infrastructure.Helpers;
using NewRepublique.PetaPoco.Repository.POCOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace NewRepublique.Garofalo.Campaign.Controllers
{
    public class HomeController : Controller
    {
        private IEmailHelper _emailHelper;

        public HomeController(IEmailHelper emailHelper)
        {
            _emailHelper = emailHelper;
        }

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Error404()
        {
            return View();
        }

        public ActionResult Error500()
        {
            return View("Error");
        }
    }
}
