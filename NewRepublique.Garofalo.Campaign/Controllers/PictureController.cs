﻿using AutoMapper;
using NewRepublique.BusinessLogic;
using NewRepublique.Data.Service;
using NewRepublique.Garofalo.Campaign.Models.DTOs;
using NewRepublique.Garofalo.Campaign.Models.ViewModels;
using NewRepublique.Infrastructure.Helpers;
using NewRepublique.PetaPoco.Repository.POCOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;

namespace NewRepublique.Garofalo.Campaign.Controllers
{
    public class PictureController : ApiController
    {
        private IPosterService _posterService;
        private IPosterLogic _posterLogic;
        private IEmailHelper _emailHelper;

        public PictureController(IPosterService posterService, IPosterLogic posterLogic, IEmailHelper emailHelper)
        {
            _posterService = posterService;
            _posterLogic = posterLogic;
            _emailHelper = emailHelper;
        }

        /// <summary>
        /// Post encoded image to http://localhost:33327/api/Picture/Post
        /// </summary>
        /// <returns></returns>
        public string Post([FromBody] GeneratedPosterModel postedData)
        {
            try
            {
                var encodedImage = postedData.poster.Replace("data:image/png;base64,", "");
                var currentDate = DateTime.Now;
                var imageUrl = "";
                var cdnImageUrl = "";
                var cdnThumbnailUrl = "";
                int newPosterId = 0;

                PosterModel posterModel = new PosterModel()
                {
                    CreatedOn = currentDate,
                    IsFlaggedForInappropriate = false,
                    IsVisible = true,
                    Title = string.IsNullOrEmpty(postedData.title) ? "Poster" : postedData.title,
                    Views = 0,
                    Entrant = new EntrantModel
                    {
                        CreatedBy = postedData.author,
                        CreatedOn = currentDate,
                        FacebookId = postedData.userid,
                        FirstName = postedData.author
                    }
                };

                var imageStream = _posterLogic.ConvertDataToImageStream(encodedImage);
                var imageName = string.Format("{0}_{1}_{2}_{3}_{4}_{5}_{6}.png",
                    posterModel.Entrant.FacebookId,
                    currentDate.Day,
                    currentDate.Month,
                    currentDate.Year,
                    currentDate.Hour,
                    currentDate.Minute,
                    currentDate.Second);

                if (_posterLogic.UploadToCDN(imageStream[0],
                        ConfigurationHelper.Instance.CdnImageFolderLocation, imageName,
                        ConfigurationHelper.Instance.CdnBucketName)
                    &&
                    _posterLogic.UploadToCDN(imageStream[1],
                        string.Format("{0}/thumbnail", ConfigurationHelper.Instance.CdnImageFolderLocation), imageName,
                        ConfigurationHelper.Instance.CdnBucketName)
                    //&&
                    //_posterLogic.UploadToCDN(imageStream[2],
                    //    string.Format("{0}/fb", ConfigurationHelper.Instance.CdnImageFolderLocation), imageName,
                    //    ConfigurationHelper.Instance.CdnBucketName)
                    )
                {


                    //upload successful
                    cdnImageUrl = string.Format("{0}{1}/{2}", ConfigurationHelper.Instance.CdnUrl, ConfigurationHelper.Instance.CdnImageFolderLocation, imageName);
                    cdnThumbnailUrl = string.Format("{0}{1}/thumbnail/{2}", ConfigurationHelper.Instance.CdnUrl, ConfigurationHelper.Instance.CdnImageFolderLocation, imageName);
                    posterModel.ImageUrl = cdnImageUrl;
                    posterModel.ThumbnailUrl = cdnThumbnailUrl;
                    newPosterId = _posterService.Insert(Mapper.Map<PosterModel, Poster>(posterModel));

                    if (newPosterId <= 0)
                    {
                        _emailHelper.SendEmail(ConfigurationHelper.Instance.FromErrorEmail, ConfigurationHelper.Instance.ToErrorEmail, "newPosterId = _posterService.Insert(Mapper.Map<PosterModel, Poster>(posterModel)) - FAILED", "Error: Garofalo Poster Website - Controller: Picture, Action: Post");
                    }
                }
                else
                {
                    //upload failed
                }

                //imageUrl = string.Format("{0}://{1}/Gallery/ShowPoster?id={2}&autoDisplayModal=true", Request.RequestUri.Scheme, Request.RequestUri.Authority, newPosterId);



                return newPosterId.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }        
    }
}
