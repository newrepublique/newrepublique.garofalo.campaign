﻿using AutoMapper;
using NewRepublique.Data.Service;
using NewRepublique.Garofalo.Campaign.Models.DTOs;
using NewRepublique.Garofalo.Campaign.Models.ViewModels;
using NewRepublique.Infrastructure.Helpers;
using NewRepublique.PetaPoco.Repository.POCOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace NewRepublique.Garofalo.Campaign.Controllers
{
    public class GalleryController : Controller
    {
        private IPosterService _posterService;
        private IEmailHelper _emailHelper;
        public GalleryController(IPosterService posterService, IEmailHelper emailHelper)
        {
            _posterService = posterService;
            _emailHelper = emailHelper;
        }
        public ActionResult Index(string orderBy)
        {
            TempData["pageNumber"] = null;
            ViewBag.orderby = string.IsNullOrEmpty(orderBy) ? "mostRecent" : orderBy;
            ViewBag.posterid = 0;
            ViewBag.total = _posterService.GetPosterCount(true);
            return View();
        }

        public ActionResult ShowPoster(int? id, bool? autoDisplayModal)
        {
            ViewBag.total = _posterService.GetPosterCount(true);
            TempData["pageNumber"] = null;
            ViewBag.orderby = "mostRecent";
            ViewBag.posterid = 0;
            ViewBag.autodisplaymodal = false;

            if (id == null)
                return View("Index");
            else
            {                
                ViewBag.autodisplaymodal = true;
                ViewBag.posterid = id;               

                return View("Index");
            }
        }

        public ActionResult Posters(string orderBy)
        {
            try
            {
                var model = new PostersViewModel();
                int pageNumber = TempData["pageNumber"] == null ? 1 : int.Parse(TempData["pageNumber"].ToString());
                var order = string.IsNullOrEmpty(orderBy) ? "mostRecent" : orderBy;
                ViewBag.orderby = order;
                var result = GetPosters(order, pageNumber);

                model.PostersToShow = result;
                model.PageNumber = pageNumber;
                pageNumber = pageNumber + 1;

                TempData["pageNumber"] = pageNumber;
                ViewBag.orderby = orderBy;
                

                return View("_Posters", model);
            }
            catch(Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine(ex.Message);
                sb.AppendLine(", Stacktrace: ");
                sb.AppendLine(ex.StackTrace);

                _emailHelper.SendEmail(ConfigurationHelper.Instance.FromErrorEmail, ConfigurationHelper.Instance.ToErrorEmail, sb.ToString(), "Error: Garofalo Poster Website - Gallery");

                return PartialView("_Error");
            }            
        }

        public IEnumerable<PosterModel> GetPosters(string order, int pageNumber)
        { 
            if(order == "mostRecent")
            {
                return Mapper.Map<IEnumerable<Poster>, IEnumerable<PosterModel>>
                (_posterService.GetPostersForGalleryOrderByCreateDate(pageNumber, 3, true));
            }
            else
            {
                return Mapper.Map<IEnumerable<Poster>, IEnumerable<PosterModel>>
                (_posterService.GetPostersForGalleryOrderByViews(pageNumber, 3, true));
            }
        }

        public PartialViewResult GetPoster(int id, string orderBy)
        {
            try
            {
                var result = _posterService.GetById(id);
                if (result != null)
                {
                    var poster = Mapper.Map<Poster, PosterModel>(result);

                    if(!poster.IsVisible)
                    {
                        ViewBag.showerror = 1;
                        return PartialView("_ModalPoster");
                    }

                    result.Views = poster.Views + 1;
                    _posterService.Update(result);

                    var posterViewModel = new PosterViewModel()
                    {
                        CreatedOn = poster.CreatedOn,
                        EntrantId = poster.EntrantId,
                        Id = poster.Id,
                        ImageUrl = poster.ImageUrl,
                        IsFlaggedForInappropriate = poster.IsFlaggedForInappropriate,
                        IsVisible = poster.IsVisible,
                        ModifiedBy = poster.ModifiedBy,
                        ModifiedOn = poster.ModifiedOn,
                        Title = poster.Title,
                        Views = poster.Views,
                        AuthorName = poster.Entrant.FirstName
                    };

                    PosterModalModel pmm = new PosterModalModel();

                    if (orderBy == "mostRecent")
                    {
                        pmm = Mapper.Map<PosterModalCreatedOn, PosterModalModel>(_posterService.GetPreviousAndNextPosterIdsOrderByCreatedOnDesc(id).FirstOrDefault());
                    }
                    else
                    {
                        pmm = Mapper.Map<PosterModalViews, PosterModalModel>(_posterService.GetPreviousAndNextPosterIdsOrderByViewsDesc(id).FirstOrDefault());
                    }

                    posterViewModel.PreviousPosterId = pmm.PreviousValue;
                    posterViewModel.NextPosterId = pmm.NextValue;
                    
                    //Original
                    var posterUrlForFB = string.Format("{0}://{1}/Gallery/ShowPoster?id={2}&autoDisplayModal=true", Request.Url.Scheme, Request.Url.Authority, id);

                    //var posterUrlForFB = string.Format("{0}://{1}/Gallery/Show?id={2}", Request.Url.Scheme, Request.Url.Authority, id);
                    
                    posterViewModel.PosterUrlForFB = posterUrlForFB;
                    ViewBag.posterurlforfb = posterUrlForFB;
                    ViewBag.imageurlfromcdn = poster.ImageUrl;
                    ViewBag.thumbnail = poster.ThumbnailUrl;
                    ViewBag.imageforfb = poster.ImageUrl.Replace("thumbnail", "fb");
                    ViewBag.orderby = orderBy;

                    ViewBag.skunk = string.Format("https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fec2-54-79-1-210.ap-southeast-2.compute.amazonaws.com%2FGallery%2FShow%3Fid%3D{0}", id);

                    return PartialView("_ModalPoster", posterViewModel);
                }
                else
                    return PartialView("_ModalPoster");
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine(ex.Message);
                sb.AppendLine(", Stacktrace: ");
                sb.AppendLine(ex.StackTrace);

                _emailHelper.SendEmail(ConfigurationHelper.Instance.FromErrorEmail, ConfigurationHelper.Instance.ToErrorEmail, sb.ToString(), "Error: Garofalo Poster Website - Gallery");

                return PartialView("_Error");
            }             
        }

        public PartialViewResult FlagForInappropriateness(int id)
        {
            try
            {
                var result = _posterService.GetById(id);
                if (result != null)
                {
                    result.IsFlaggedForInappropriate = true;
                    result.ModifiedOn = DateTime.Now;
                    var updateResult = _posterService.Update(result);

                    if (updateResult > 0)
                    {
                        ViewBag.flagmessage = "Poster has been flagged";
                    }
                    else
                    {
                        ViewBag.flagmessage = "An error occurred trying to flag this poster.";
                    }
                }
                else
                {
                    ViewBag.flagmessage = "An error occurred trying to flag this poster.";
                }
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine(ex.Message);
                sb.AppendLine(", Stacktrace: ");
                sb.AppendLine(ex.StackTrace);

                _emailHelper.SendEmail(ConfigurationHelper.Instance.FromErrorEmail, ConfigurationHelper.Instance.ToErrorEmail, sb.ToString(), "Error: Garofalo Poster Website - Gallery");

                ViewBag.flagmessage = "An error occured while processing your request";
            }

            return PartialView("_Appropriateness");
        }

        public ActionResult Show(int id)
        {
            var model = new PosterViewModel();
            var result = _posterService.GetById(id);
            
            if (result != null)
            {
                ViewBag.url = string.Format("{0}://{1}/Gallery/ShowPoster?id={2}&autoDisplayModal=true", Request.Url.Scheme, Request.Url.Authority, id);
                ViewBag.imageurl = result.ImageUrl;
            }

            return View(model);
        }

        public ActionResult ShowTest()
        {
            return View();
        }
	}
}