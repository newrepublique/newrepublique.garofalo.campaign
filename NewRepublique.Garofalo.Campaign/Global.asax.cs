﻿using NewRepublique.Infrastructure.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace NewRepublique.Garofalo.Campaign
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            UnityConfig.RegisterComponents();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Bootstrapper.RegisterMappings();
            Bootstrapper.Initialise();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine(ex.Message);
            sb.AppendLine(", Stacktrace: ");
            sb.AppendLine(ex.StackTrace);

            if (ex is HttpException)
            {
                var httpException = ex as HttpException;

                if (httpException.GetHttpCode() != 404)
                {
                    SendErrorEmail(ConfigurationHelper.Instance.FromErrorEmail, ConfigurationHelper.Instance.ToErrorEmail, sb.ToString());
                }
            }
            else
                SendErrorEmail(ConfigurationHelper.Instance.FromErrorEmail, ConfigurationHelper.Instance.ToErrorEmail, sb.ToString());
        }

        public void SendErrorEmail(string fromEmail, string toEmail, string body)
        {
            SmtpClient serv = new SmtpClient("localhost");
            MailMessage msg = new MailMessage(fromEmail, toEmail);
            msg.Body = body;
            msg.Subject = "Error: Garofalo Poster Website";
            msg.BodyEncoding = System.Text.Encoding.ASCII;
            msg.IsBodyHtml = true;
            serv.UseDefaultCredentials = true;
            serv.Send(msg);
        }
    }
}
