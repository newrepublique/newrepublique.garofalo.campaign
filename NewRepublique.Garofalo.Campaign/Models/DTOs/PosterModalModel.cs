﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewRepublique.Garofalo.Campaign.Models.DTOs
{
    public class PosterModalModel
    {
        public int PreviousValue { get; set; }
        public int CurrentValue { get; set; }
        public int NextValue { get; set; }
    }
}