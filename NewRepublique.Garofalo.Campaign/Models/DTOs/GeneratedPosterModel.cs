﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewRepublique.Garofalo.Campaign.Models.DTOs
{
    public class GeneratedPosterModel
    {
        public string author { get; set; }
        public string userid { get; set; }
        public string poster { get; set; }
        public string title { get; set; }
        
    }
}