﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewRepublique.Garofalo.Campaign.Models.ViewModels
{
    public class PosterViewModel
    {
        public int Id { get; set; }
        public int EntrantId { get; set; }
        public string Title { get; set; }
        public string ImageUrl { get; set; }
        public int Views { get; set; }
        public bool IsFlaggedForInappropriate { get; set; }
        public bool IsVisible { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public string ThumbnailUrl { get; set; }
        public int PreviousPosterId { get; set; }
        public int NextPosterId { get; set; }
        public string PosterUrlForFB { get; set; }
        public int Share { get; set; }

        public string AuthorName { get; set; }
    }
}