﻿using Amazon.S3;
using Amazon.S3.Transfer;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewRepublique.BusinessLogic
{
    public interface IPosterLogic
    {
        List<MemoryStream> ConvertDataToImageStream(string encodedString);
        bool UploadToCDN(MemoryStream imageMemoryStream, string folderLocation, string imageName, string bucketName);
    }

    public class PosterLogic : IPosterLogic
    {
        public List<MemoryStream> ConvertDataToImageStream(string encodedString)
        {
            List<MemoryStream> images = new List<MemoryStream>();

            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(encodedString);

            byte[] thumbnailBytes = CreateThumbnail(imageBytes);

            //byte[] fbBytes = CreateFBImage(imageBytes);

            // Convert byte[] to Image
            MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);            
            ms.Write(imageBytes, 0, imageBytes.Length);

            MemoryStream msThumbnail = new MemoryStream(thumbnailBytes, 0, thumbnailBytes.Length);
            msThumbnail.Write(thumbnailBytes, 0, thumbnailBytes.Length);

            //MemoryStream msFbImage = new MemoryStream(fbBytes, 0, fbBytes.Length);
            //msFbImage.Write(fbBytes, 0, fbBytes.Length);

            images.Add(ms);
            images.Add(msThumbnail);
            //images.Add(msFbImage);

            return images;
        }


        public bool UploadToCDN(MemoryStream imageMemoryStream, string folderLocation, string imageName, string bucketName)
        {
            try
            {
                AmazonS3Config config = new AmazonS3Config();
                config.Timeout = new TimeSpan(0, 1, 0);
                config.RegionEndpoint = Amazon.RegionEndpoint.USEast1;
                AmazonS3Client amazonClient = new AmazonS3Client(config);
                TransferUtility fileTransferUtility = new TransferUtility(amazonClient);                

                TransferUtilityUploadRequest uploadRequest = new TransferUtilityUploadRequest();
                uploadRequest.InputStream = imageMemoryStream;
                uploadRequest.BucketName = bucketName;
                uploadRequest.Key = string.Format("{0}/{1}", folderLocation, imageName);
                uploadRequest.CannedACL = S3CannedACL.PublicRead;

                fileTransferUtility.Upload(uploadRequest);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public byte[] CreateThumbnail(byte[] PassedImage)
        {
            byte[] ReturnedThumbnail;

            using (MemoryStream StartMemoryStream = new MemoryStream(),
                                NewMemoryStream = new MemoryStream())
            {
                // write the string to the stream  
                StartMemoryStream.Write(PassedImage, 0, PassedImage.Length);

                // create the start Bitmap from the MemoryStream that contains the image  
                Bitmap startBitmap = new Bitmap(StartMemoryStream);

                // set thumbnail height and width proportional to the original image.  
                int newHeight = 312;
                int newWidth = 234;
                //double HW_ratio;
                //if (startBitmap.Height > startBitmap.Width)
                //{
                //    HW_ratio = (double)((double)newHeight / (double)startBitmap.Height);
                //    newWidth = (int)(HW_ratio * (double)startBitmap.Width);
                //}
                //else
                //{
                //    HW_ratio = (double)((double)newWidth / (double)startBitmap.Width);
                //    newHeight = (int)(HW_ratio * (double)startBitmap.Height);
                //}

                // create a new Bitmap with dimensions for the thumbnail.  
                Bitmap newBitmap = new Bitmap(newWidth, newHeight);

                // Copy the image from the START Bitmap into the NEW Bitmap.  
                // This will create a thumnail size of the same image.  
                newBitmap = ResizeImage(startBitmap, newWidth, newHeight);

                // Save this image to the specified stream in the specified format.  
                newBitmap.Save(NewMemoryStream, System.Drawing.Imaging.ImageFormat.Png);

                // Fill the byte[] for the thumbnail from the new MemoryStream.  
                ReturnedThumbnail = NewMemoryStream.ToArray();
            }

            // return the resized image as a string of bytes.  
            return ReturnedThumbnail;
        }

        public byte[] CreateFBImage(byte[] PassedImage)
        {
            byte[] ReturnedThumbnail;

            using (MemoryStream StartMemoryStream = new MemoryStream(),
                                NewMemoryStream = new MemoryStream())
            {
                // write the string to the stream  
                StartMemoryStream.Write(PassedImage, 0, PassedImage.Length);

                // create the start Bitmap from the MemoryStream that contains the image  
                Bitmap startBitmap = new Bitmap(StartMemoryStream);

                // set thumbnail height and width proportional to the original image.  
                int newHeight = 600;
                int newWidth = 600;
                //double HW_ratio;
                //if (startBitmap.Height > startBitmap.Width)
                //{
                //    HW_ratio = (double)((double)newHeight / (double)startBitmap.Height);
                //    newWidth = (int)(HW_ratio * (double)startBitmap.Width);
                //}
                //else
                //{
                //    HW_ratio = (double)((double)newWidth / (double)startBitmap.Width);
                //    newHeight = (int)(HW_ratio * (double)startBitmap.Height);
                //}

                // create a new Bitmap with dimensions for the thumbnail.  
                Bitmap newBitmap = new Bitmap(newWidth, newHeight);

                // Copy the image from the START Bitmap into the NEW Bitmap.  
                // This will create a thumnail size of the same image.  
                newBitmap = ResizeImage(startBitmap, newWidth, newHeight);

                // Save this image to the specified stream in the specified format.  
                newBitmap.Save(NewMemoryStream, System.Drawing.Imaging.ImageFormat.Png);

                // Fill the byte[] for the thumbnail from the new MemoryStream.  
                ReturnedThumbnail = NewMemoryStream.ToArray();
            }

            // return the resized image as a string of bytes.  
            return ReturnedThumbnail;
        }

        // Resize a Bitmap  
        private static Bitmap ResizeImage(Bitmap image, int width, int height)
        {
            Bitmap resizedImage = new Bitmap(width, height);
            using (Graphics gfx = Graphics.FromImage(resizedImage))
            {
                gfx.DrawImage(image, new Rectangle(0, 0, width, height),
                    new Rectangle(0, 0, image.Width, image.Height), GraphicsUnit.Pixel);
            }
            return resizedImage;
        } 
    }    
}
