﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewRepublique.PetaPoco.Repository.POCOs
{
    [TableName("Entrant")]
    [PrimaryKey("Id")]
    public class Entrant
    {
        public int Id { get; set; }
        public string FacebookId { get;set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        [Ignore]
        public IList<Poster> Posters { get; set; }
    }
}
