﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewRepublique.PetaPoco.Repository.POCOs
{
    [TableName("vw_PosterPreviousNext_Views")]
    public class PosterModalViews
    {
        public int PreviousValue { get; set; }
        public int CurrentValue { get; set; }
        public int NextValue { get; set; }
    }
}
