using System.Collections.Generic;
using PetaPoco;
using System;
using NewRepublique.PetaPoco.Repository.POCOs;

namespace NewRepublique.PetaPoco.Repository.Business.Interface
{
    public interface IRepository
    {
        T Single<T>(object primaryKey);

        IEnumerable<T> Query<T>();
        List<T> Fetch<T>();
        Page<T> PagedQuery<T>(long pageNumber, long itemsPerPage, string sql, params object[] args);
        int Insert(object itemToAdd);
        int Update(object itemToUpdate, object primaryKeyValue);
        int Delete<T>(object primaryKeyValue);
        List<T> FetchOneToMany<T, T1>(Func<T, object> key, Sql Sql);
        List<T> FetchOneToMany<T, T1>(Func<T, object> key, string sql, params object[] args);
        List<T> FetchManyToOne<T, T1>(Func<T, object> key, string sql, params object[] args);
        List<T> FetchManyToOne<T, T1, T2>(Func<T, object> key, string sql, params object[] args);
        List<T> FetchManyToOne<T, T1, T2, T3>(Func<T, object> key, string sql, params object[] args);
        int ExcuteSQL(string sql, params object[] args);
        IEnumerable<T> Query<T>(Sql sql);
        List<TPassType> Fetch<TPassType>(string sql, params object[] args);
        void StartTransaction();
        void CompleteTransaction();
        void AbortTransaction();
        int ExecuteScalar(string sql, params object[] args);
        IEnumerable<T> Query<T>(string sql, params object[] args);
    }
}
