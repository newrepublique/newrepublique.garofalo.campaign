using System;
using System.Collections.Generic;

using PetaPoco;

using NewRepublique.PetaPoco.Repository.Business.Interface;
using NewRepublique.Infrastructure.Helpers;
using NewRepublique.PetaPoco.Repository.POCOs;

namespace NewRepublique.PetaPoco.Repository.Business
{
    public class PetaPocoRepository : Relator, IRepository
    {
        private Database db;
        public PetaPocoRepository()
        {
            db = new Database(ConfigurationHelper.Instance.ConnectionStringName);
        }
        public TPassType Single<TPassType>(object primaryKey)
        {
            return db.Single<TPassType>(primaryKey);
        }
        public IEnumerable<TPassType> Query<TPassType>()
        {
            var pd = Database.PocoData.ForType(typeof(TPassType));
            var sql = "SELECT * FROM " + pd.TableInfo.TableName;
            return db.Query<TPassType>(sql);
        }
        public IEnumerable<TPassType> Query<TPassType>(string sql, params object[] args)
        {
            return db.Query<TPassType>(sql, args);
        }
        public List<TPassType> Fetch<TPassType>()
        {
            var pd = Database.PocoData.ForType(typeof(TPassType));
            var sql = "SELECT * FROM " + pd.TableInfo.TableName;
            return db.Fetch<TPassType>(sql);
        }
        public List<TPassType> Fetch<TPassType>(string sql, params object[] args)
        {
            return db.Fetch<TPassType>(sql, args);
        }
        public Page<TPassType> PagedQuery<TPassType>(long pageNumber, long itemsPerPage, string sql, params object[] args)
        {
            return db.Page<TPassType>(pageNumber, itemsPerPage, sql, args) as Page<TPassType>;
        }
        public Page<TPassType> PagedQuery<TPassType>(long pageNumber, long itemsPerPage, Sql sql)
        {
            return db.Page<TPassType>(pageNumber, itemsPerPage, sql) as Page<TPassType>;
        }
        public int Insert(object poco)
        {
            return Convert.ToInt32(db.Insert(poco));
        }
        public int Insert(string tableName, string primaryKeyName, bool autoIncrement, object poco)
        {
            return Convert.ToInt32(db.Insert(tableName, primaryKeyName, autoIncrement, poco));
        }
        public int Insert(string tableName, string primaryKeyName, object poco)
        {
            return Convert.ToInt32(db.Insert(tableName, primaryKeyName, poco));
        }
        public int Update(object poco)
        {
            return db.Update(poco);
        }
        public int Update(object poco, object primaryKeyValue)
        {
            return db.Update(poco, primaryKeyValue);
        }
        public int Update(string tableName, string primaryKeyName, object poco)
        {
            return db.Update(tableName, primaryKeyName, poco);
        }
        public int Update(object poco, IEnumerable<string> columns)
        {
            return db.Update(poco, columns);
        }
        public int Delete<TPassType>(object pocoOrPrimaryKey)
        {
            return db.Delete<TPassType>(pocoOrPrimaryKey);
        }

        public List<T> FetchOneToMany<T, T1>(Func<T, object> key, Sql Sql)
        {
            var relator = new Relator();
            return db.Fetch<T, T1, T>((a, b) => relator.OneToMany(a, b, key), Sql);
        }

        public List<T> FetchOneToMany<T, T1>(Func<T, object> key, string sql, params object[] args)
        {
            return db.FetchOneToMany<T, T1>(key, new Sql(sql, args));
        }

        public List<T> FetchManyToOne<T, T1>(Func<T, object> key, string sql, params object[] args)
        {
            return db.FetchManyToOne<T, T1>(key, new Sql(sql, args));
        }

        public List<T> FetchManyToOne<T, T1, T2>(Func<T, object> key, string sql, params object[] args)
        {
            return db.FetchManyToOne<T, T1, T2>(key, new Sql(sql, args));
        }

        public List<T> FetchManyToOne<T, T1, T2, T3>(Func<T, object> key, string sql, params object[] args)
        {
            return db.FetchManyToOne<T, T1, T2, T3>(key, new Sql(sql, args));
        }

        public int ExcuteSQL(string sql, params object[] args)
        {
            return db.Execute(sql, args);
        }

        public IEnumerable<T> Query<T>(Sql sql)
        {
            return db.Query<T>(sql);
        }

        public void StartTransaction()
        {
            db.BeginTransaction();
        }

        public void CompleteTransaction()
        {
            db.CompleteTransaction();
        }

        public void AbortTransaction()
        {
            db.AbortTransaction();
        }

        public int ExecuteScalar(string sql, params object[] args)
        {
            return db.ExecuteScalar<int>(sql, args);
        }
    }
}