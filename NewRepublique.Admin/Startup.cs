﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NewRepublique.Admin.Startup))]
namespace NewRepublique.Admin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
