﻿using AutoMapper;
using NewRepublique.Admin.Models.DTOs;
using NewRepublique.Data.Service;
using NewRepublique.PetaPoco.Repository.POCOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace NewRepublique.Admin.Security
{
    public class CustomRoleProvider : RoleProvider
    {
        private IUserService _userService;

        public CustomRoleProvider()
            : this(DependencyResolver.Current.GetService<IUserService>())
        { }

        public CustomRoleProvider(IUserService userService)
        {
            _userService = userService;
        }

        public override string[] GetRolesForUser(string username)
        {
            var result = _userService.Get(username);

            if (result != null)
            {
                var user = Mapper.Map<User, UserModel>(result);

                return new[] { user.Role };
            }
            else
            {
                return new string[] { };
            }
        }

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }
    }
}