﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewRepublique.Admin.Models.DTOs
{
    public class EntrantModel
    {
        public int Id { get; set; }
        public string FacebookId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public IList<PosterModel> PosterModels { get; set; }
    }
}