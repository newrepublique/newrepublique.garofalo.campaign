﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewRepublique.Admin.Models.DTOs
{
    public class UserModel
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public string CreatedBy { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public bool Locked { get; set; }
        public DateTime? LockedDate { get; set; }
    }
}