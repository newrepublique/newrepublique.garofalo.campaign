﻿using MvcPaging;
using NewRepublique.Admin.Models.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NewRepublique.Admin.Models.ViewModels
{
    public class PostersViewModel
    {
        public IPagedList<PosterModel> Posters { get; set; }

        [Required(ErrorMessage = "*")]
        public DateTime FromDate { get; set; }

        [Required(ErrorMessage = "*")]
        public DateTime ToDate { get; set; }

        [Required(ErrorMessage = "*")]
        public string OrderBy { get; set; }

        public int PageNumber { get; set; }

        public int PageSize { get; set; }

        public int TotalCount { get; set; }

        public int TotalPostersCreated { get; set; }
        public int TotalViews { get; set; }
        public int TotalShares { get; set; }
    }
}