﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NewRepublique.Admin.Models.ViewModels
{
    public class CreateUserViewModel
    {
        public int UserId { get; set; }

        [Required(ErrorMessage = "*")]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "Username must be between 3 and 20 characters")]
        public string Username { get; set; }

        [Required(ErrorMessage = "*")]
        [StringLength(500, MinimumLength = 6, ErrorMessage = "Password must be greater than 6 characters")]
        public string Password { get; set; }

        [Compare("Password", ErrorMessage = "Does not match Password")]
        [Display(Name = "Confirm Password")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "*")]
        public string Role { get; set; }

        [Required(ErrorMessage = "*")]
        [StringLength(50, ErrorMessage = "Firstname must be less than 50 characters")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Last Name")]
        [StringLength(50, ErrorMessage = "Lastname must be less than 50 characters")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "*")]
        [RegularExpression(@"\b[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.+-]+\.[a-zA-Z]{2,6}\b", ErrorMessage = "Invalid email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "*")]
        public bool Locked { get; set; }

        [Display(Name = "Locked When")]
        public DateTime? LockedDate { get; set; }

        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }

        [Display(Name = "Created On")]
        public DateTime CreatedOn { get; set; }

        [Display(Name = "Last Updated By")]
        public string LastModifiedBy { get; set; }

        [Display(Name = "Last Updated On")]
        public DateTime? LastModifiedOn { get; set; }
    }
}