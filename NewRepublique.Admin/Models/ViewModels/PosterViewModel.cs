﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewRepublique.Admin.Models.ViewModels
{
    public class PosterViewModel
    {
        public int Id { get; set; }
        public int EntrantId { get; set; }
        public string Title { get; set; }
        public string ImageUrl { get; set; }
        public int Views { get; set; }
        public bool IsFlaggedForInappropriate { get; set; }
        public bool IsVisible { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public string LastModifiedBy { get; set; }
        public string ThumbnailUrl { get; set; }
        public int Share { get; set; }

        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int PageNumber { get; set; }
        public string OrderBy { get; set; }
    }
}