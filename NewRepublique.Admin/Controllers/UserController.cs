﻿using AutoMapper;
using NewRepublique.Admin.Models.DTOs;
using NewRepublique.Admin.Models.ViewModels;
using NewRepublique.Data.Service;
using NewRepublique.Infrastructure.Helpers;
using NewRepublique.PetaPoco.Repository.POCOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NewRepublique.Admin.Controllers
{
    public class UserController : Controller
    {
        private IUserService _userService;
        private IHashingHelper _hashingHelper;

        public UserController(IUserService userService, IHashingHelper hashingHelper)
        {
            _userService = userService;
            _hashingHelper = hashingHelper;
        }
        public ActionResult Index()
        {
            var model = GetDataForIndex();
            return View(model);
        }

        public ActionResult Details(int id)
        {
            var result = _userService.Get(id);

            if (result != null)
            {
                var x = Mapper.Map<User, UserModel>(result);

                UserViewModel model = new UserViewModel
                {
                    CreatedBy = x.CreatedBy,
                    CreatedOn = x.CreatedOn,
                    LastModifiedOn = x.LastModifiedOn,
                    Email = x.Email,
                    FirstName = x.FirstName,
                    UserId = x.UserId,
                    LastName = x.LastName,
                    Locked = x.Locked,
                    LockedDate = x.LockedDate,
                    Password = x.Password,
                    Role = x.Role,
                    LastModifiedBy = x.LastModifiedBy,
                    Username = x.Username
                };

                return View(model);
            }
            else
            {
                return View();
            }
        }

        public ActionResult Edit(int id)
        {
            var result = _userService.Get(id);

            if (result != null)
            {
                var user = Mapper.Map<User, UserModel>(result);

                UserViewModel model = new UserViewModel
                {
                    CreatedBy = user.CreatedBy,
                    CreatedOn = user.CreatedOn,
                    LastModifiedOn = user.LastModifiedOn,
                    Email = user.Email,
                    FirstName = user.FirstName,
                    UserId = user.UserId,
                    LastName = user.LastName,
                    Locked = user.Locked,
                    LockedDate = user.LockedDate,
                    Password = user.Password,
                    Role = user.Role,
                    LastModifiedBy = user.LastModifiedBy,
                    Username = user.Username
                };

                return View(model);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult Edit(UserViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var adminUser = Mapper.Map<User, UserModel>(_userService.Get(model.UserId));

                    adminUser.LastModifiedOn = DateTime.Now;
                    adminUser.Email = model.Email;
                    adminUser.FirstName = model.FirstName;
                    adminUser.LastName = model.LastName;
                    adminUser.Locked = model.Locked;
                    adminUser.Role = model.Role;
                    adminUser.Username = model.Username;
                    adminUser.LastModifiedBy = User.Identity.Name;

                    if (!adminUser.Locked)
                        adminUser.LockedDate = null;
                    else
                    {
                        if (adminUser.LockedDate == null)
                            adminUser.LockedDate = DateTime.Now;
                    }

                    if (!string.IsNullOrEmpty(model.Password))
                    {
                        adminUser.Password = _hashingHelper.HashStringInSHA256(model.Password);
                    }

                    if (_userService.Update(Mapper.Map<UserModel, User>(adminUser)) > 0)
                    {
                        //update successful
                        ViewBag.message = "User saved";
                    }
                    else
                    {
                        //unsuccessful
                        ViewBag.message = "Save unsuccessful";
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.message = ex.Message;
                }
            }

            return View();
        }

        public List<UserViewModel> GetDataForIndex()
        {
            List<UserViewModel> model = new List<UserViewModel>();

            var result = _userService.GetAll();

            if (result.Count() > 0)
            {
                var adminUsers = Mapper.Map<IEnumerable<User>, IEnumerable<UserModel>>(result);

                foreach (var x in adminUsers)
                {
                    UserViewModel auvm = new UserViewModel
                    {
                        CreatedBy = x.CreatedBy,
                        CreatedOn = x.CreatedOn,
                        LastModifiedOn = x.LastModifiedOn,
                        Email = x.Email,
                        FirstName = x.FirstName,
                        UserId = x.UserId,
                        LastName = x.LastName,
                        Locked = x.Locked,
                        LockedDate = x.LockedDate,
                        Password = x.Password,
                        Role = x.Role,
                        LastModifiedBy = x.LastModifiedBy,
                        Username = x.Username
                    };

                    model.Add(auvm);
                }
            }

            return model;
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(CreateUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var user = new UserModel
                    {
                        CreatedOn = DateTime.Now,
                        CreatedBy = User.Identity.Name,
                        Email = model.Email,
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        Locked = model.Locked,
                        Role = model.Role,
                        Username = model.Username,
                        Password = _hashingHelper.HashStringInSHA256(model.Password)
                    };

                    if (_userService.Create(Mapper.Map<UserModel, User>(user)) > 0)
                    {
                        ViewBag.message = "User created";
                    }
                    else
                    {
                        ViewBag.message = "User not created";
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.message = ex.Message;
                }
            }

            return View(model);
        }

        public ActionResult Delete(int id)
        {
            if (_userService.Delete(id) > 0)
                ViewBag.message = "User deleted.";
            else
                ViewBag.message = "Delete failed";

            var model = GetDataForIndex();

            return View("Index", model);
        }

    }
}