﻿using AutoMapper;
using NewRepublique.Admin.Models.DTOs;
using NewRepublique.Admin.Models.ViewModels;
using NewRepublique.Data.Service;
using NewRepublique.PetaPoco.Repository.POCOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcPaging;

namespace NewRepublique.Admin.Controllers
{
    public class PosterController : Controller
    {
        private IPosterService _posterService;

        public PosterController(IPosterService posterService)
        {
            _posterService = posterService;
        }

        [Authorize]
        public ActionResult Index()
        {
            PostersViewModel model = new PostersViewModel();
            var currentDate = DateTime.Now;
            
            int totalCount = 0;
            model.PageSize = 20;
            model.FromDate = new DateTime(2014, currentDate.AddMonths(-1).Month, 1);
            model.ToDate = new DateTime(2014, currentDate.AddMonths(1).Month, 1);
            model.OrderBy = "CreatedOn";
            model.PageNumber = 1;

            if (ModelState.IsValid)
            {
                var result = _posterService.GetPostersForAdmin(model.PageNumber, model.PageSize, model.FromDate, model.ToDate, model.OrderBy, ref totalCount);
                if (result.Count > 0)
                {
                    var stats = _posterService.GetStats(model.FromDate, model.ToDate);
                    ViewBag.totalposters = stats.ElementAt(0);
                    ViewBag.totalviews = stats.ElementAt(1);
                    ViewBag.totalshares = stats.ElementAt(2);
                }
                else
                {
                    ViewBag.totalposters = 0;
                    ViewBag.totalviews = 0;
                    ViewBag.totalshares = 0;
                }

                model.TotalCount = totalCount;
                model.Posters = Mapper.Map<IEnumerable<Poster>, IEnumerable<PosterModel>>(result).ToPagedList(model.PageNumber-1, model.PageSize, totalCount);
            }
            
            return View(model);
        }

        [Authorize]
        public ActionResult Retrieve(PostersViewModel model, int? page)
        {
            int totalCount = 0;
            model.PageSize = 20;

            if (page != null)
                model.PageNumber = page.Value;
            else
                model.PageNumber = 1;

            if (ModelState.IsValid)
            {
                var result = _posterService.GetPostersForAdmin(model.PageNumber, model.PageSize, model.FromDate, model.ToDate, model.OrderBy, ref totalCount);
                if(result.Count > 0)
                {
                    var stats = _posterService.GetStats(model.FromDate, model.ToDate);
                    ViewBag.totalposters = stats.ElementAt(0);
                    ViewBag.totalviews = stats.ElementAt(1);
                    ViewBag.totalshares = stats.ElementAt(2);
                }
                else
                {
                    ViewBag.totalposters = 0;
                    ViewBag.totalviews = 0;
                    ViewBag.totalshares = 0;
                }
                

                model.TotalCount = totalCount;
                model.Posters = Mapper.Map<IEnumerable<Poster>, IEnumerable<PosterModel>>(result).ToPagedList(model.PageNumber - 1, model.PageSize, totalCount);

                
            }

            return View("Index", model);
        }

        [HttpGet]
        public ActionResult Edit(int id, DateTime fromDate, DateTime toDate, string orderBy, int page)
        {
            PosterViewModel model = new PosterViewModel();

            var result = Mapper.Map<Poster, PosterModel>(_posterService.GetById(id));

            model.Id = result.Id;
            model.CreatedOn = result.CreatedOn;
            model.EntrantId = result.EntrantId;
            model.ImageUrl = result.ImageUrl;
            model.IsFlaggedForInappropriate = result.IsFlaggedForInappropriate;
            model.IsVisible = result.IsVisible;
            model.LastModifiedBy = result.ModifiedBy;
            model.LastModifiedOn = result.ModifiedOn;
            model.Title = result.Title;
            model.Views = result.Views;
            model.ThumbnailUrl = result.ThumbnailUrl;

            model.FromDate = fromDate;
            model.ToDate = toDate;
            model.OrderBy = orderBy;
            model.PageNumber = page;

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(PosterViewModel model)
        {
            if (ModelState.IsValid)
            {
                PosterModel pm = new PosterModel
                {
                    CreatedOn = model.CreatedOn,
                    EntrantId = model.EntrantId,
                    Id = model.Id,
                    ImageUrl = model.ImageUrl,
                    IsFlaggedForInappropriate = model.IsFlaggedForInappropriate,
                    IsVisible = model.IsVisible,
                    ModifiedBy = User.Identity.Name,
                    ModifiedOn = DateTime.Now,
                    Title = model.Title,
                    Views = model.Views,
                    ThumbnailUrl = model.ThumbnailUrl
                };


                if (_posterService.Update(Mapper.Map<PosterModel, Poster>(pm)) > 0)
                    ViewBag.result = "Update Sucessful";
                else
                    ViewBag.result = "Update Failed";
            }

            return View(model);
        }
    }
}