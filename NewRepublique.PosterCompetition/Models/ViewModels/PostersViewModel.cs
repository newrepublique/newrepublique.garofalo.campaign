﻿using NewRepublique.PosterCompetition.Models.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewRepublique.PosterCompetition.Models.ViewModels
{
    public class PostersViewModel
    {
        public IEnumerable<PosterModel> PostersToShow { get; set; }
        public int PageNumber { get; set; }
    }
}