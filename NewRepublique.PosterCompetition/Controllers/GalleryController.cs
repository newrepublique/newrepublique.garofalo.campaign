﻿using AutoMapper;
using NewRepublique.Data.Service;
using NewRepublique.PetaPoco.Repository.POCOs;
using NewRepublique.PosterCompetition.Models.DTOs;
using NewRepublique.PosterCompetition.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NewRepublique.PosterCompetition.Controllers
{
    public class GalleryController : Controller
    {
        private IPosterService _posterService;

        public GalleryController(IPosterService posterService)
        {
            _posterService = posterService;
        }

        public ActionResult Index()
        {
            TempData["pageNumber"] = null;
            ViewBag.total = _posterService.GetTotalPagesForGallery(3, true);

            return View();
        }

        public ActionResult Posters()        
        {
            var model = new PostersViewModel();
            int pageNumber = TempData["pageNumber"] == null ? 1 : int.Parse(TempData["pageNumber"].ToString());

            var result = Mapper.Map<IEnumerable<Poster>, IEnumerable<PosterModel>>( _posterService.GetPostersForGalleryOrderByCreateDate(pageNumber, 3, true));

            model.PostersToShow = result;
            model.PageNumber = pageNumber;
            pageNumber = pageNumber + 1;
            
            TempData["pageNumber"] = pageNumber;

            return View("_Posters", model);
        }
	}
}