﻿using System.Web;
using System.Web.Mvc;

namespace NewRepublique.PosterCompetition
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
