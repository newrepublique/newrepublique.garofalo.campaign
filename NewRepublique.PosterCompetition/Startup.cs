﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NewRepublique.PosterCompetition.Startup))]
namespace NewRepublique.PosterCompetition
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
