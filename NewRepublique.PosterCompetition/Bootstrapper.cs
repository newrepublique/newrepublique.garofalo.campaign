using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc4;
using NewRepublique.PetaPoco.Repository.Business.Interface;
using NewRepublique.PetaPoco.Repository.Business;
using NewRepublique.Data.Service;
using NewRepublique.BusinessLogic;
using AutoMapper;
using NewRepublique.PetaPoco.Repository.POCOs;
using NewRepublique.PosterCompetition.Models.DTOs;

namespace NewRepublique.PosterCompetition
{
    public static class Bootstrapper
    {
        public static IUnityContainer Initialise()
        {
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));

            return container;
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();    
            RegisterTypes(container);
            container.RegisterType<IRepository, PetaPocoRepository>();
            container.RegisterType<IPosterService, PosterService>();
            container.RegisterType<IPosterLogic, PosterLogic>();

            return container;
        }

        public static void RegisterTypes(IUnityContainer container)
        {

        }

        public static void RegisterMappings()
        {
            Mapper.CreateMap<Entrant, EntrantModel>();
            Mapper.CreateMap<EntrantModel, Entrant>();

            Mapper.CreateMap<Poster, PosterModel>();
            Mapper.CreateMap<PosterModel, Poster>();
        }
    }
}