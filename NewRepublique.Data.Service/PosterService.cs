﻿using NewRepublique.PetaPoco.Repository.Business.Interface;
using NewRepublique.PetaPoco.Repository.POCOs;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewRepublique.Data.Service
{
    public interface IPosterService
    {
        Poster GetById(int id);
        IEnumerable<Poster> Get();
        IEnumerable<Poster> GetPostersForGalleryOrderByCreateDate(int pageNumber, int numberOfItems, bool visibility);
        IEnumerable<Poster> GetPostersForGalleryOrderByViews(int pageNumber, int numberOfItems, bool visibility);
        int Insert(Poster newPoster);
        int GetTotalPagesForGallery(int numberOfItems, bool visibility);
        IEnumerable<Poster> GetPosters(int pageNumber, int numberOfItems, DateTime fromDate, DateTime toDate, string orderBy, ref int totalCount);
        List<Poster> GetPostersForAdmin(int pageNumber, int numberOfItems, DateTime fromDate, DateTime toDate, string orderBy, ref int totalCount);
        int Update(Poster posterToUpdate);
        IEnumerable<PosterModalCreatedOn> GetPreviousAndNextPosterIdsOrderByCreatedOnDesc(int posterId);
        IEnumerable<PosterModalViews> GetPreviousAndNextPosterIdsOrderByViewsDesc(int posterId);
        int GetPosterCount(bool visibility);

        /// <summary>
        /// returns a List of 3 integer values: total posters, total views and total shares
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        List<int> GetStats(DateTime fromDate, DateTime toDate);
    }

    public class PosterService : IPosterService
    {
        private IRepository _repository { get; set; }
        public PosterService(IRepository repository)
        {
            _repository = repository;
        }
        public Poster GetById(int id)
        {
            return _repository.FetchManyToOne<Poster, Entrant>(x => x.Id,
                "SELECT * FROM Poster INNER JOIN Entrant ON Poster.EntrantId = Entrant.Id WHERE Poster.Id = @0", id).FirstOrDefault();
        }
        public IEnumerable<Poster> Get()
        {
            return _repository.Fetch<Poster>();
        }
        public IEnumerable<Poster> GetPostersForGalleryOrderByCreateDate(int pageNumber, int numberOfItems, bool visibility)
        {
            int numberOfRowsToSkip = 0;
            
            if(pageNumber == 1)
            {
                numberOfRowsToSkip = 0;
            }
            else
            {
                numberOfRowsToSkip = (pageNumber - 1) * numberOfItems;
            }            

            return _repository.FetchManyToOne<Poster, Entrant>(x => x.Id, 
                "SELECT * FROM Poster INNER JOIN Entrant ON Poster.EntrantId = Entrant.Id WHERE IsVisible = @0 ORDER BY Poster.CreatedOn DESC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY",
                visibility, numberOfRowsToSkip, numberOfItems );
        }
        public IEnumerable<Poster> GetPostersForGalleryOrderByViews(int pageNumber, int numberOfItems, bool visibility)
        {
            int numberOfRowsToSkip = 0;

            if (pageNumber == 1)
            {
                numberOfRowsToSkip = 0;
            }
            else
            {
                numberOfRowsToSkip = (pageNumber - 1) * numberOfItems;
            }   

            return _repository.FetchManyToOne<Poster, Entrant>(x => x.Id,
                "SELECT * FROM Poster INNER JOIN Entrant ON Poster.EntrantId = Entrant.Id WHERE IsVisible = @0 ORDER BY Views DESC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY",
                visibility, numberOfRowsToSkip, numberOfItems);
        }

        public IEnumerable<Poster> GetPosters(int pageNumber, int numberOfItems, DateTime fromDate, DateTime toDate, string orderBy, ref int totalCount)
        {
            int numberOfRowsToSkip = 0;

            if (pageNumber == 1)
            {
                numberOfRowsToSkip = 0;
            }
            else
            {
                numberOfRowsToSkip = (pageNumber - 1) * numberOfItems;
            }

            var countSql = "SELECT Count(*) FROM Poster INNER JOIN Entrant ON Poster.EntrantId = Entrant.Id WHERE Poster.CreatedOn BETWEEN @0 AND @1";
            var sql = "SELECT * FROM Poster INNER JOIN Entrant ON Poster.EntrantId = Entrant.Id WHERE Poster.CreatedOn BETWEEN @0 AND @1 ORDER BY Poster.{0} DESC OFFSET @2 ROWS FETCH NEXT @3 ROWS ONLY";

            sql = string.Format(sql, orderBy);
            totalCount = _repository.ExecuteScalar(countSql, fromDate, toDate); 

            return _repository.FetchManyToOne<Poster, Entrant>(x => x.Id, sql,
                fromDate, toDate, numberOfRowsToSkip, numberOfItems);
        }

        public List<Poster> GetPostersForAdmin(int pageNumber, int numberOfItems, DateTime fromDate, DateTime toDate, string orderBy, ref int totalCount)
        {
            int numberOfRowsToSkip = 0;

            if (pageNumber == 1)
            {
                numberOfRowsToSkip = 0;
            }
            else
            {
                numberOfRowsToSkip = (pageNumber - 1) * numberOfItems;
            }

            var countSql = "SELECT Count(*) FROM Poster INNER JOIN Entrant ON Poster.EntrantId = Entrant.Id WHERE Poster.CreatedOn BETWEEN @0 AND @1";
            var sql = "SELECT * FROM Poster INNER JOIN Entrant ON Poster.EntrantId = Entrant.Id WHERE Poster.CreatedOn BETWEEN @0 AND @1 ORDER BY Poster.{0} DESC OFFSET @2 ROWS FETCH NEXT @3 ROWS ONLY";

            sql = string.Format(sql, orderBy);
            totalCount = _repository.ExecuteScalar(countSql, fromDate, toDate);

            return _repository.FetchManyToOne<Poster, Entrant>(x => x.Id, sql,
                fromDate, toDate, numberOfRowsToSkip, numberOfItems);
        }

        public int GetTotalPagesForGallery(int numberOfItems, bool visibility)        
        {
            var totalCount = _repository.ExecuteScalar("SELECT Count(*) FROM Poster WHERE IsVisible = @0", 1);

            if(totalCount < 3)
            {
                totalCount = 3;
            }
            return totalCount / numberOfItems;
        }

        public int Insert(Poster newPoster)
        {
            try
            {
                _repository.StartTransaction();

                int entrantId = 0;
                var entrantResult = _repository.Fetch<Entrant>("WHERE FacebookId = @0", newPoster.Entrant.FacebookId);
                if(entrantResult.Count == 0)
                {
                    //new Entrant
                    entrantId = _repository.Insert(newPoster.Entrant);
                }
                else
                {
                    //existing Entrant
                    entrantId = entrantResult.FirstOrDefault().Id;
                }

                newPoster.EntrantId = entrantId;
                var posterId = _repository.Insert(newPoster);

                _repository.CompleteTransaction();

                return posterId;
            }
            catch(Exception ex)
            {
                _repository.AbortTransaction();
                return -1;
            }
        }

        public int Update(Poster posterToUpdate)
        {
            return _repository.Update(posterToUpdate, posterToUpdate.Id);
        }

        public IEnumerable<PosterModalCreatedOn> GetPreviousAndNextPosterIdsOrderByCreatedOnDesc(int posterId)
        {
            return _repository.Fetch<PosterModalCreatedOn>("WHERE CurrentValue = @0", posterId);
        }

        public IEnumerable<PosterModalViews> GetPreviousAndNextPosterIdsOrderByViewsDesc(int posterId)
        {
            return _repository.Fetch<PosterModalViews>("WHERE CurrentValue = @0", posterId);
        }

        public int GetPosterCount(bool visibility)
        {
            return _repository.ExecuteScalar("SELECT Count(*) FROM Poster WHERE IsVisible = @0", visibility);
        }

        /// <summary>
        /// returns a List of 3 integer values: total posters, total views and total shares
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        public List<int> GetStats(DateTime fromDate, DateTime toDate)
        {
            var totalPosters = _repository.ExecuteScalar("SELECT COUNT(*) FROM Poster WHERE CreatedOn BETWEEN @0 AND @1", fromDate, toDate);
            var totalViews = _repository.ExecuteScalar("SELECT SUM(Views) FROM Poster WHERE CreatedOn BETWEEN @0 AND @1", fromDate, toDate);
            var totalShares = _repository.ExecuteScalar("SELECT SUM(Share) FROM Poster WHERE CreatedOn BETWEEN @0 AND @1", fromDate, toDate);

            List<int> totals = new List<int>();

            totals.Add(totalPosters);
            totals.Add(totalViews);
            totals.Add(totalShares);

            return totals;
        }
    }
}
