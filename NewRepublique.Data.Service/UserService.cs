﻿using NewRepublique.PetaPoco.Repository.Business.Interface;
using NewRepublique.PetaPoco.Repository.POCOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewRepublique.Data.Service
{
    public interface IUserService
    {
        User Get(string username, string password);
        User Get(string username);
        int Update(User userToUpdate);
        IEnumerable<User> GetAll();
        User Get(int id);
        int Create(User user);
        int Delete(int id);
    }
    public class UserService : IUserService
    {
        private IRepository _repository { get; set; }
        public UserService(IRepository repository)
        {
            _repository = repository;
        }

        public User Get(string username, string password)
        {
            return _repository.Fetch<User>("WHERE Username = @0 AND Password = @1", username, password).FirstOrDefault();
        }

        public User Get(string username)
        {
            return _repository.Fetch<User>("WHERE Username = @0", username).FirstOrDefault();
        }

        public int Update(User userToUpdate)
        {
            return _repository.Update(userToUpdate, userToUpdate.UserId);
        }

        public IEnumerable<User> GetAll()
        {
            return _repository.Fetch<User>();
        }

        public User Get(int id)
        {
            return _repository.Fetch<User>("WHERE UserId = @0", id).FirstOrDefault();
        }

        public int Create(User user)
        {
            return _repository.Insert(user);
        }

        public int Delete(int id)
        {
            return _repository.Delete<User>(id);
        }
    }
}
