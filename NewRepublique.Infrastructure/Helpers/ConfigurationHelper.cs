﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewRepublique.Infrastructure.Helpers
{
    public class ConfigurationHelper
    {
        private const string CONNECTION_STRING_NAME = "ConnectionStringName";
        private const string CDN_IMAGE_FOLDER_LOCATION = "CdnImageFolderLocation";
        private const string CDN_BUCKET_NAME = "CdnBucketName";
        private const string CDN_URL = "CdnUrl";

        private const string TO_ERROR_EMAIL = "ToErrorEmail";
        private const string FROM_ERROR_EMAIL = "FromErrorEmail";

        //--------------------------------------------------------------------------------------------------------------------
        private static ConfigurationHelper _instance;
        public static ConfigurationHelper Instance
        {
            get
            {
                lock (typeof(ConfigurationHelper))
                {
                    if (_instance == null)
                        _instance = new ConfigurationHelper();

                    return _instance;
                }
            }
        }

        private string GetConfiguration(string name)
        {
            return System.Configuration.ConfigurationManager.AppSettings[name];
        }


        private string _connectionStringName;
        public string ConnectionStringName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_connectionStringName))
                {
                    _connectionStringName = GetConfiguration(CONNECTION_STRING_NAME);
                }

                return _connectionStringName;
            }
        }


        private string _cdnImageFolderLocation;
        public string CdnImageFolderLocation
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_cdnImageFolderLocation))
                {
                    _cdnImageFolderLocation = GetConfiguration(CDN_IMAGE_FOLDER_LOCATION);
                }

                return _cdnImageFolderLocation;
            }
        }


        private string _cdnBucketName;
        public string CdnBucketName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_cdnBucketName))
                {
                    _cdnBucketName = GetConfiguration(CDN_BUCKET_NAME);
                }

                return _cdnBucketName;
            }
        }


        private string _cdnUrl;
        public string CdnUrl
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_cdnUrl))
                {
                    _cdnUrl = GetConfiguration(CDN_URL);
                }

                return _cdnUrl;
            }
        }

        private string _toErrorEmail;
        public string ToErrorEmail
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_toErrorEmail))
                {
                    _toErrorEmail = GetConfiguration(TO_ERROR_EMAIL);
                }

                return _toErrorEmail;
            }
        }

        private string _fromErrorEmail;
        public string FromErrorEmail
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_fromErrorEmail))
                {
                    _fromErrorEmail = GetConfiguration(FROM_ERROR_EMAIL);
                }

                return _fromErrorEmail;
            }
        }
    }
}
