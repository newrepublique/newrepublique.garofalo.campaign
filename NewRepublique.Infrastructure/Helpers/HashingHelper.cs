﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace NewRepublique.Infrastructure.Helpers
{
    public interface IHashingHelper
    {
        string HashStringInSHA256(string inputString);
    }

    public class HashingHelper : IHashingHelper
    {
        /// <summary>
        /// Taken from http://stackoverflow.com/questions/12416249/hashing-a-string-with-sha256
        /// </summary>
        /// <param name="inputString">The string to be hashed</param>
        /// <returns>Hashed string in SHA256</returns>
        public string HashStringInSHA256(string inputString)
        {
            SHA256Managed crypt = new SHA256Managed();
            string hash = String.Empty;
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(inputString), 0, Encoding.UTF8.GetByteCount(inputString));
            foreach (byte bit in crypto)
            {
                hash += bit.ToString("x2");
            }

            return hash;
        }
    }
}
