﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace NewRepublique.Infrastructure.Helpers
{
    public interface IEmailHelper
    {
        void SendEmail(string fromEmail, string toEmail, string body, string subject);
    }

    public class EmailHelper : IEmailHelper
    {
        public void SendEmail(string fromEmail, string toEmail, string body, string subject)
        {
            SmtpClient serv = new SmtpClient("localhost");
            MailMessage msg = new MailMessage(fromEmail, toEmail);
            msg.Body = body;
            msg.Subject = subject;
            msg.BodyEncoding = System.Text.Encoding.ASCII;
            msg.IsBodyHtml = true;
            serv.UseDefaultCredentials = true;
            serv.Send(msg);
        }
    }
}
